﻿using UnityEngine;
using System.Collections;

public class AI : MonoBehaviour {


	public float rotationSpeed = 0.1f;
	public GameObject player;
	public float speed = 3f;
	private Vector2 m_moveDirection = Vector2.zero;
	public float aggroDistance;
	public bool ground;
	public float yVelocity = 0;
	public LayerMask layer;
	public int flip;
	public float fallingSpeed = 10;
	public Transform knockBackPosition;

	void Start () {


		player = FindObjectOfType<Movement>().gameObject;
		
	}
	
	void LateUpdate(){





		if(Vector3.Distance(transform.position, player.transform.position) < aggroDistance)
		{
			if(transform.position.x > player.transform.position.x)
			{
				transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, 0, 0), rotationSpeed);
				transform.Translate(new Vector3(-speed * Time.deltaTime, 0, 0), Space.World);
				flip = 0;
			}
			else
			{
				transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, 180, 0), rotationSpeed);
				transform.Translate(new Vector3(speed * Time.deltaTime, 0, 0), Space.World);
				flip = 1;
			}
		}
		/*
		RaycastHit2D hitfloor = Physics2D.Raycast(transform.position, -Vector2.up, 0.1f);
		if (hitfloor.collider != null) {
			transform.Translate(-Vector2.up * Time.deltaTime * speed * 2);
		}
		*/
		GetComponent<Rigidbody2D>().velocity = Vector2.zero;
		Vector2 dist = m_moveDirection;
		
		if(Physics2D.CircleCast(new Vector2(transform.position.x,transform.position.y - 0.5f),0.5f,-Vector2.up,0.1f,layer))
			ground = true;
		else
			ground = false;


		yVelocity += Physics2D.gravity.y * fallingSpeed * Time.deltaTime;

		if (ground && yVelocity < 0) 
		{
			yVelocity = Physics2D.gravity.y  * Time.deltaTime;
			//yVelocity = -0.01f;
		}
		
		dist.y = yVelocity * Time.deltaTime;

		m_moveDirection = transform.TransformDirection(m_moveDirection);
		
		GetComponent<Rigidbody2D>().velocity = dist;

	}

	void OnTriggerEnter2D(Collider2D other)
	{
		switch (flip) {
		case 0:
			Debug.Log("d");
			//other.rigidbody2D.AddForce(-Vector2.up * 5000);
			//other.rigidbody2D.AddForce(Vector2.right * 5000);
			break;
		case 1:
			Debug.Log("d");
			//other.rigidbody2D.AddForce(Vector2.up * 5000);
			//other.rigidbody2D.AddForce(Vector2.right * 5000);

			//other.transform.position = Vector2.Lerp(other.transform.position, 
			break;
		}
	}
}

﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class AnimationScript : MonoBehaviour {

	private Animator anim;
	public enum Type{

		player,
		enemy
	}
	public Type type;
	private float speed;
	private float moveDirection;
	private Movement movement;
	private Transform m_transform;
	private Aim aim;
	private Stats stats;
	private ActiveSpells activeSpells;

	void Awake () 
	{
		m_transform = GetComponent<Transform> ();
		anim = GetComponent<Animator>();

		if(type == Type.player)
		{
			stats = GetComponentInParent<Stats>();
			activeSpells = GetComponentInParent<ActiveSpells>();
			movement = m_transform.root.GetComponent<Movement>();
			aim = m_transform.root.GetComponentInChildren<Aim>();
		}
	}

	void Update () 
	{
		switch(type)
		{
		case Type.player:
			PlayerAimations();
			break;
		case Type.enemy:
			EnemyAnimations();
			break;
		}
	}
	void PlayerAimations()
	{

		if(aim.GetHorizontalAimDirectionNormalized() >= 0.5f)
			m_transform.localScale = new Vector3(0.6f,0.6f,0.6f);
		else if(aim.GetHorizontalAimDirectionNormalized() <= 0.5f)
			m_transform.localScale = new Vector3(-0.6f,0.6f,0.6f);

		anim.SetFloat ("speed", movement.GetCurrentSpeed ());
		anim.SetBool("grounded", movement.Grounded());
		anim.SetBool("isLevitating", movement.IsLevitating());

		if(activeSpells.castSpell)
		{
			anim.SetTrigger("castSpell");
			activeSpells.castSpell = false;
		}

		if(stats.isDead)
		{
			anim.SetTrigger("death");
			movement.allowMovement = false;
		}
	}
	void EnemyAnimations()
	{

	}
}
﻿using UnityEngine;
using System.Collections;

public class BasicAI : MonoBehaviour {

	public enum State{

		idle,
		engage,

	};
	public enum MoveDirection{

		right,
		left

	};
	public Animator anim;
	public CircleCollider2D circleCol;
	public State currentState;
	public MoveDirection dir;
	public float interval;
	public bool wander = false;
	public Stats stats;

	public int meleeDmg;
	public float attackTimer;
	public float agroRange;
	public float meleeRange;
	public float moveDurationMin;
	public float moveDurationMax;
	public float speed;
	public float chaseSpeed;
	public float fallingSpeed;

	public LayerMask layerMask;
	private Transform m_transform;
	private Vector2 dist;
	private float yVelocity;
	private Vector2 m_moveDirection = Vector2.zero;
	private bool ground = false;

	public float colliderRadius;
	private float timer;
	private bool doIdleTimer;
	private bool allowMoving = false;
	private float moveTimer = 0;
	private float attack;

	public Transform target;

	public bool isMoving;
	
	void Update()
	{
		CheckState ();
		SearchForEnemy();
		if(doIdleTimer)
		{
			DoIdleTimer();
			if(allowMoving)
			{
				float randomTime = Random.Range(moveDurationMin,moveDurationMax);
				MoveAI(dir,randomTime);
			}
		}

		anim.SetBool ("isWalking", isMoving);

		if(!allowMoving)
		{
			isMoving = false;
		}

		if(stats.isDead)
		{
			anim.SetTrigger ("Die");
			this.enabled = false;
		}
	}

	void DoIdleTimer()
	{
		if(timer >= interval)
		{
			timer = 0;
			if(currentState == State.idle)
			{
				DoIdle();
			}
		}
		
		timer += Time.deltaTime;
	}
	void MoveAI(MoveDirection moveDir,float time)
	{
		//CheckDir();
		switch(moveDir)
		{
		case MoveDirection.right:
			isMoving = true;
			m_transform.Translate(-Vector2.right* speed* Time.deltaTime);
			break;
		case MoveDirection.left:
			isMoving = true;
			m_transform.Translate(Vector2.right* speed* Time.deltaTime);
			break;
		}

		moveTimer += Time.deltaTime;
		
		if(moveTimer>=time)
		{
			moveTimer = 0;
			allowMoving = false;
		}
	}
	void Awake()
	{
		if(GetComponent<CircleCollider2D>())
			circleCol = GetComponent<CircleCollider2D>();
		if(GetComponent<Stats>())
			stats = GetComponent<Stats>();
		if(GetComponent<Animator>())
			anim = GetComponent<Animator>();

		m_transform = GetComponent<Transform> ();
	}
	void FixedUpdate()
	{
		//DoMovement();
	}
	void CheckState()
	{
		switch(currentState)
		{
		case State.idle:
			doIdleTimer = true;
			break;
		case State.engage:
			doIdleTimer = false;
			DoEngage();
			break;
		}
	}
	void DoIdle()
	{
		int rmd = Random.Range(0,2);
		if(rmd == 0)
		{
			SetRandomDirection();
			CheckDir();
		}

		if(wander)
		{
			int move = Random.Range (0, 2);
			if(move == 0)
			{
				allowMoving = true;
			}
		}
	}
	void DoEngage()
	{
		ChaseTarget();
	}

	void SearchForEnemy()
	{
		RaycastHit2D hit = new RaycastHit2D();
		switch(dir)
		{
		case MoveDirection.right:
			hit = Physics2D.Raycast (GetComponent<BoxCollider2D>().offset + new Vector2(m_transform.position.x,m_transform.position.y), -Vector2.right, agroRange,layerMask);
			break;
		case MoveDirection.left:
			hit = Physics2D.Raycast (GetComponent<BoxCollider2D>().offset + new Vector2(m_transform.position.x,m_transform.position.y), Vector2.right, agroRange,layerMask);
			break;
		}

		if(hit.collider != null)
		{
			GameObject go = hit.collider.gameObject;
			if(go.GetComponent<Movement>())
			{
				currentState = State.engage;
				target = go.transform;
			}
			else
			{
				currentState = State.idle;
				target = null;
			}
		}
		else
		{
			currentState = State.idle;
			target = null;
		}
	}
	void ChaseTarget()
	{
		isMoving = true;
		switch(dir)
		{
		case MoveDirection.right:
			CheckDir();
			if(Vector2.Distance(m_transform.position,target.transform.position) > meleeRange)
			{
				m_transform.Translate (-Vector2.right * chaseSpeed * Time.deltaTime);
			}
			else if(Vector2.Distance(m_transform.position,target.transform.position) <= meleeRange)
			{
				isMoving = false;
				DoCombat();
			}
			break;
		case MoveDirection.left:
			CheckDir();
			if(Vector2.Distance(m_transform.position,target.transform.position) > meleeRange)
			{
				m_transform.Translate (Vector2.right * chaseSpeed * Time.deltaTime);
			}
			else if(Vector2.Distance(m_transform.position,target.transform.position) <= meleeRange)
			{
				isMoving = false;
				DoCombat();
			}
			break;
		}
	}
	void FlipAI()
	{
		//dir = (MoveDirection.left < MoveDirection.righ)? MoveDirection.right : MoveDirection.left;
		if(dir == MoveDirection.left)
			dir = MoveDirection.right;
		else if(dir == MoveDirection.right)
			dir = MoveDirection.left;

		CheckDir();
	}
	void SetRandomDirection()
	{
		int moveDir = Random.Range(0,2);
		switch(moveDir)
		{
		case 0:
			dir = MoveDirection.right;
			break;
		case 1:
			dir = MoveDirection.left;
			break;
		}

	}
	void CheckDir()
	{
		switch(dir)
		{
		case MoveDirection.right:
			m_transform.localScale = new Vector3(Mathf.Abs(m_transform.localScale.x),m_transform.localScale.y,m_transform.localScale.z);
			break;
		case MoveDirection.left:
			m_transform.localScale = new Vector3(-Mathf.Abs(m_transform.localScale.x),m_transform.localScale.y,m_transform.localScale.z);
			break;
		}
	}
	void DoCombat()
	{
		if(attack >= attackTimer)
		{
			attack = 0;
			DoMeleeHit();
		}
		attack += Time.deltaTime;
	}
	void DoMeleeHit()
	{
		Debug.Log (meleeDmg);
		anim.SetTrigger ("MeleeHit");
		target.GetComponent<Stats>().TakeMeleeDmg(meleeDmg);
	}
//	void OnDrawGizmos()
//	{
//		Gizmos.DrawSphere (new Vector2(transform.position.x,transform.position.y - 0.25f),colliderRadius); 
//	}
	void DoMovement()
	{
		GetComponent<Rigidbody2D>().velocity = Vector2.zero;
		dist = m_moveDirection;
		circleCol.radius = colliderRadius;

		if(Physics2D.CircleCast(new Vector2(transform.position.x,transform.position.y - 0.5f),colliderRadius,-Vector2.up,0.2f,layerMask))
			ground = true;
		else
			ground = false;

		yVelocity += Physics2D.gravity.y * fallingSpeed * Time.deltaTime;
		
		if (ground && yVelocity < 0) 
		{
			yVelocity = Physics2D.gravity.y  * Time.deltaTime;
		}
		
		dist.y = yVelocity * Time.deltaTime;

		m_moveDirection = transform.TransformDirection(m_moveDirection);
		
		GetComponent<Rigidbody2D>().velocity = dist;
	}
}
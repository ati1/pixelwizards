﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Chatlog : MonoBehaviour {
	
	public GameObject cl_canvas;
	//public Text[] cl_Texts;
	public Text cl_Text;
	public int cl_lines = 0;
	private int cl_maxLines = 6;

	public string[] cl_commandList;
	/*
	 * 0 = Help command
	 * 1 = Toggle chat command
	 * 2 = Version command
	 * 3 = Toggle your coordinates
	 *
	 *
	 */

	public InputField cl_chatInput;

	//temporary name, probably
	private string player_name = "Matias The Wizard";
	
	//public Vector2 cl_chatStartPos = new Vector2(0, 0);
	
	// Use this for initialization
	void Start () {
		
		cl_chatInput.DeactivateInputField();
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		if(!cl_chatInput.IsInteractable())
		{
			if(Input.GetKeyUp(KeyCode.T))
			{
				if(!cl_chatInput.IsInteractable()) {
					cl_chatInput.interactable = true;
					cl_chatInput.ActivateInputField();
				}
				else {
					cl_chatInput.interactable = false;
					cl_chatInput.DeactivateInputField();
					cl_chatInput.text = "";
				}
			}
		}
	}

	//HANDLES THE INPUTFIELD KINDA, A BIT BUGGY DNO WHY :(((( the above fixed it a bit for some reason idk why :OO
	void LockChatInput()
	{
		if(!cl_chatInput.IsInteractable()) {
			cl_chatInput.interactable = true;
			cl_chatInput.ActivateInputField();
		}
		else {
			cl_chatInput.interactable = false;
			cl_chatInput.DeactivateInputField();
			cl_chatInput.text = "";
		}
	}

	//CHECKS IF THE COMMAND IS VALID
	void DoCommandCheck(string command)
	{
		bool found = false;
		LockChatInput();
		for(int i = 0; i < cl_commandList.Length; i++)
		{
			if(cl_commandList[i] == command)
			{
				found = true;
				DoCommands(i);
			}
		}
		if(found == false)
			SendErrorMessageToPlayer("Invalid command.");
	}

	//ADD COMMANDS AND THE FUNCTIONALITY HERE
	void DoCommands(int id)
	{

	/*
	 * 0 = Help command
	 * 1 = Toggle chat command
	 * 2 = Version command
	 * 3 = Toggle your coordinates
	 *
	 *
	 */
		switch(id)
		{
		case 0:
		{
			SendClientMessage("yellow", "!!HELP!!");
			SendClientMessage("yellow", "Welcome to the Wizard Land, available commands:");
			for(int i = 0; i < cl_commandList.Length; i++)
				SendClientMessage("yellow", cl_commandList[i]);
			break;
		}
		case 1:
		{
			//HERE GOGO
			break;																	
		}
		case 2:
		{
			//HERE ASWELL
			break;
		}
		case 3:
		{
			//AND HERE XDDDD
			break;
		}
		}
	}


	//BUILDS UP THE PLAYER MESSAGE BECAUSE INPUTFIELD HAS PROBLEMS WITH RICHTEXT SO MLEL :PPPP
	public void BuildPlayerString()
	{
		if(cl_chatInput.text.StartsWith("/"))
		{
			DoCommandCheck(cl_chatInput.text);
		}
		else
		{
			string msg = cl_chatInput.text;
			SendPlayerMessage(msg);
		}
	}

	public void SendPlayerMessage(string message)
	{
		cl_Text.text += "\n<color=white>" + player_name + ": " + message + "</color>";
		LockChatInput();
	}
	
	//!! int playerid !! THIS IS USED TO DISPLAY ANYTHING TO ONE PLAYER, USE THIS WHEN YOU ARE NOT SURE WHAT TO DO :PPPP
	public void SendClientMessage(string color, string message)
	{
		cl_Text.text += "\n<color=" + color + ">" + message + "</color>";
		//cl_Text.text = message;
	}

	//WHAT NPC SAYS
	public void SendNPCText(string npc_name, string message)
	{
		cl_lines++;
		//cl_Text.color = Color.white;
		//cl_Text.fontStyle = FontStyle.Italic;
		cl_Text.text += "\n<i><color=white>" + npc_name + ": " + message + "</color> </i>";
	}

	//NOTIFICATION TO ALL, LIKE SERVER RESTARTS AND STUFF Y'KNOW
	public void SendNotificationToAll(string message)
	{
		cl_lines++;
		//cl_Text.color = Color.yellow;
		cl_Text.text += "\n<color=yellow>[NOTIFICATION] " + message + "</color>";
	}


	//ERROR TO ALL, IDK WHY, BUT SURE WHY NOT
	public void SendErrorMessageToAll(string message)
	{
		cl_lines++;
		cl_Text.text += "\n<color=red>[ERROR] " + message + "</color>";
	}

	//TO ONE PLAYER
	public void SendErrorMessageToPlayer(string message)
	{
		cl_lines++;
		cl_Text.text += "\n<color=red>[ERROR] " + message + "</color>";
	}


	//A MESSAGE TO ALL PLAYERS, CAN BE USED AS WHATEVER
	public void SendClientMessageToAll(Color color, string message)
	{
		cl_lines++;
		cl_Text.text += "\n" + message;
		/*cl_lines++;
		Text t;
		t = Instantiate(cl_Texts[cl_lines]) as Text;
		t.transform.SetParent(transform);
		//t.transform.tag = "t" + cl_lines;
		//t.transform.position = cl_chatStartPos;
		cl_Texts[cl_lines] = t;
		if(cl_lines > 0)
		{
			for(int i = 1; i < cl_maxLines; i++)
			{
				cl_Texts[i].transform.position = new Vector2(cl_Texts[i].transform.position.x, 
				                                             cl_Texts[i].transform.position.y + 6f);
			}
		}
		if(cl_lines >= cl_maxLines - 1)
		{
			cl_lines = 0;

		}
		cl_Texts[cl_lines].color = color;
		cl_Texts[cl_lines].text = message;
		*/
	}

	//IDK WIP :PPP
	void ClearChat()
	{
		cl_Text.text = "";
	}
}


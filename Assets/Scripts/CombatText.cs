﻿using UnityEngine;
using System.Collections;

public class CombatText : MonoBehaviour {

	public float scrollSpeed = 0;
	[HideInInspector]
	public TextMesh textMesh;
	private Transform m_transform;

	void Awake() 
	{
		m_transform = GetComponent<Transform> ();
		textMesh = GetComponent<TextMesh> ();
	}
	void Update () 
	{
		m_transform.Translate(Vector2.up * scrollSpeed * Time.deltaTime);
	}
}
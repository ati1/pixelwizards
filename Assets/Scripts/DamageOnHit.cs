﻿using UnityEngine;
using System.Collections;

public class DamageOnHit : MonoBehaviour {

	public int dmg;
	public Spell.SpellSchool dmgType;
	public float tickTimer;
	private float m_timer;
	private bool m_timerStart;

	void Update()
	{
		if(m_timerStart)
			m_timer += Time.deltaTime;
	}
//	void OnTriggerEnter2D(Collider2D col)
//	{
//		if(col.GetComponent<Stats>() && !m_onTriggerStay)
//		{
//			Stats stats = col.GetComponent<Stats>();
//			stats.TakeSpellDmg(dmg,dmgType);
//		}
//	}
	void OnTriggerStay2D(Collider2D col)
	{
		if(col.GetComponent<Stats>())
		{
			m_timerStart = true;

			if(m_timer >= tickTimer)
			{
				m_timer = 0;
				Stats stats = col.GetComponent<Stats>();
				stats.TakeSpellDmg(dmg,dmgType);
			}
		}
	}
//	void OnTriggerExit2D(Collider2D col)
//	{
//		if(col.GetComponent<Stats>())
//			m_timerStart = false;
//		m_timer = tickTimer;
//	}
}
﻿using UnityEngine;
using System.Collections;

public class DestroyOnTimer : MonoBehaviour {

	public float destroyTimer = 0;
	private float m_currentTimer = 0;

	void Awake()
	{
		m_currentTimer = destroyTimer;
	}
	void Update () 
	{
		if (m_currentTimer <= 0)
			m_currentTimer = 0;
		if (m_currentTimer == 0)
			Destroy (gameObject);
		m_currentTimer -= Time.deltaTime;
	}
}

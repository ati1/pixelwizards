﻿using UnityEngine;
using System.Collections;

public class HealingPotion : ItemDatabase.Item {
	
	public int healthPrecentage;

	public override void InitItem()
	{
//		UseItem = Heal;
		isStackable = true;
	}

	void Heal()
	{
		if(owner != null)
		{
			Stats stats = owner.GetComponent<Stats>();
			if(stats.health < stats.maxHealth)
			{
				float m_healthPresentage = healthPrecentage / 100f * (float)stats.maxHealth;
				stats.Heal(m_healthPresentage);
				currentStack--;
			}
		}
	}
}
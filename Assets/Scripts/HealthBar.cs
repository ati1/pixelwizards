﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {

	public bool showOldGUI;

	public RectTransform rectTrans;
	public Text lvlText;
	public Text hpBarText;

	public Texture2D healthBarTexture;
	public Texture2D dmgTakenTexture;
	private Stats m_stats;
	private Transform m_transform;
	public float hpBarOffsetY = 0;
	public float hpBarOffsetX = 0;
	public float healthbarLenght;
	private float m_maxHealthBarLength = 0;

	
	void Start () 
	{
		m_maxHealthBarLength = healthbarLenght;
		m_stats = GetComponent<Stats> ();
		m_transform = GetComponent<Transform> ();
	}
	public Vector2 GetRealPos(Vector2 pos)
	{
		Vector2 temp = Camera.main.WorldToScreenPoint (pos);
		return new Vector2 (temp.x, Screen.height - temp.y);
	}
	public void AdjustHealthBar()
	{
		if(hpBarText != null)
			hpBarText.text = m_stats.health +"/"+m_stats.maxHealth;

		if(m_stats.health > 0)
			healthbarLenght = (float)m_stats.health / (float)m_stats.maxHealth * m_maxHealthBarLength;
		else
			healthbarLenght = 0;

		if(!showOldGUI)
		{
			UpdateHealthBar ();
			UpdateLvlText();
		}
		
	}
	void UpdateLvlText()
	{
		lvlText.text = m_stats.exp.GetCurrentLvl().ToString();
	}
	void UpdateHealthBar()
	{
		rectTrans.sizeDelta = new Vector2 (healthbarLenght, rectTrans.sizeDelta.y);
	}
	void OnGUI()
	{
		if(showOldGUI)
		{
			GUI.Label (new Rect (GetRealPos (m_transform.position).x - hpBarOffsetX - 25, GetRealPos (m_transform.position).y - hpBarOffsetY - 7, 25, 25), m_stats.exp.currentLvl.ToString ());
			GUI.DrawTexture(new Rect(GetRealPos(m_transform.position).x - hpBarOffsetX,GetRealPos(m_transform.position).y - hpBarOffsetY, m_maxHealthBarLength, 10), dmgTakenTexture);
			GUI.DrawTexture(new Rect(GetRealPos(m_transform.position).x - hpBarOffsetX,GetRealPos(m_transform.position).y - hpBarOffsetY, healthbarLenght, 10), healthBarTexture);
		}
	}
}
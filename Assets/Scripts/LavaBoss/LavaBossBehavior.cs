﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LavaBossBehavior : MonoBehaviour {

	public bool playerInBossfight;

	public int level;
	public int health;
	public int lavaBreathDmg;
	
	public GameObject lavaBreath;
	public Transform boss;
	public Transform shootPos;
	public Transform head;
	
	public float attackTimer;
	private float m_startAttackTimer;
	
	public bool canAttack;
	
	public List<Stats> partsStats;
	private Stats[] m_parts;
	
	private Movement m_player;
	
	public Transform[] positions;
	
	private Animator m_anim;
	

	public enum Attack
	{
		LavaBreath,
		TailSlash
	};
	
	public bool facingRight;

	public GameObject ground;

	public bool victory;
	public float groundMoveTime;
	
	private Loot m_loot;
	
	public GameObject door;
	
	public Collider2D bossStartTrigger;
	
	
	public void SetAllImmune(bool value)
	{
		foreach(Stats partStat in m_parts)
		{
			partStat.SetImmune(value);
		}
	}


	void Awake()
	{
//		m_startAttackTimer = attackTimer;
//		m_parts = GetComponentsInChildren<Stats>();
//		partsStats.AddRange(m_parts);
//		SetUpHealth();
//		m_player = FindObjectOfType<Movement>();
//		lavaBreathDmg = level * 4;
//		playerInBossfight = true;
//		m_loot = GetComponent<Loot>();
//		m_anim = GetComponentInChildren<Animator>();
	}
	
	public void StartBossFight()
	{
		bossStartTrigger.gameObject.SetActive(false);
		m_startAttackTimer = attackTimer;
		m_parts = GetComponentsInChildren<Stats>();
		partsStats.AddRange(m_parts);
		SetUpHealth();
		m_player = FindObjectOfType<Movement>();
		lavaBreathDmg = level * 4;
		playerInBossfight = true;
		m_loot = GetComponent<Loot>();
		m_anim = GetComponentInChildren<Animator>();
	}
	
	void SetUpHealth()
	{
		foreach(Stats stats in partsStats)
		{
			health += stats.maxHealth;
		}
	}
	
	void LateUpdate()
	{
		if(playerInBossfight)
			DoLogic();

		if(victory)
		{
			EndBossFight();
		}
	}
	
	void DoLogic()
	{
		attackTimer -= Time.deltaTime;
		if(attackTimer <= 0)
		{
			attackTimer = m_startAttackTimer;
			
			if(canAttack)
				DoAttack(Attack.LavaBreath);
		}
		Aim();
	}
	
	void Aim()
	{
		float angleDir = AngleDir(new Vector3(boss.position.x,boss.position.y,0), new Vector3(m_player.transform.position.x,transform.position.y,0));

		if (angleDir < 0)
		{
			if(!facingRight)
			{
				boss.localScale = new Vector3(-boss.localScale.x,boss.localScale.y,boss.localScale.z);
				head.transform.localScale = new Vector3(-head.transform.localScale.x,head.transform.localScale.y,head.transform.localScale.z);
				facingRight = true;
			}


			Vector3 dir = head.transform.position - m_player.transform.position;
			float rot_z = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
			head.rotation = Quaternion.Euler(360f, 180f, -(rot_z - 170));

		}
		else if (angleDir > 0)
		{
			if(facingRight)
			{
				boss.localScale = new Vector3(Mathf.Abs(boss.localScale.x),boss.localScale.y,boss.localScale.z);
				head.transform.localScale = new Vector3(Mathf.Abs(head.transform.localScale.x),head.transform.localScale.y,head.transform.localScale.z);
				facingRight = false;
			}


			Vector3 dir = head.transform.position - m_player.transform.position;
			float rot_z = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
			head.rotation = Quaternion.Euler(0f, 0f, rot_z);

		}
	}

	public static float AngleDir(Vector2 A, Vector2 B)
	{
		return -A.x * B.y + A.y * B.x;
	}
	
	void DoAttack(Attack attack)
	{
		switch(attack)
		{
		case Attack.LavaBreath:
			m_anim.enabled = false;
			LavaBreath();
			m_anim.enabled = true;
			break;
		case Attack.TailSlash:
			break;
		}
	}
	
	void MoveBossTimer()
	{
		
	}
	
	public void CheckBossHealth()
	{
		if( partsStats.Count > 0 )
		{
			for(int i = 0;i<partsStats.Count;i++)
			{
				if(partsStats[i].isDead)
				{
					health -= 200;
					if(partsStats.Count - 1 == 0)
					{
						partsStats.Remove(partsStats[i]);
						//player won
						playerInBossfight = false;
						victory = true;
						m_anim.enabled = false;
						Rigidbody2D[] headChilds = head.GetComponentsInChildren<Rigidbody2D>();
						//head.rigidbody2D.isKinematic = false;
						for( int j = 0;j<m_parts.Length;j++ )
						{
							m_parts[j].gameObject.SetActive(false);
						}
						for( int j = 0;j<headChilds.Length;j++ )
						{
							headChilds[j].isKinematic = false;
							headChilds[j].GetComponent<Collider2D>().enabled = true;
						}
						m_loot.DropItems();
					}
					else
					{
						partsStats.Remove(partsStats[i]);				
						StartCoroutine("Submerge");
					}
				}
			}
		}
	}
	
	void LavaBreath()
	{
		StartCoroutine("DisableAnimator");
	}
	
	IEnumerator DisableAnimator()
	{
		m_anim.enabled = false;
		yield return new WaitForEndOfFrame();
		Quaternion rot = new Quaternion(shootPos.rotation.x,shootPos.rotation.y,shootPos.rotation.z,shootPos.rotation.w);
		if(facingRight)
			rot = Quaternion.Euler(0,0,-(shootPos.eulerAngles.z - 180));
		
		
		GameObject temp = GameObject.Instantiate(lavaBreath,shootPos.position,rot) as GameObject;
		FireBall bossFB = temp.GetComponent<FireBall>();
		bossFB.isEnemySpell = true;
		bossFB.dmg = lavaBreathDmg;
		bossFB.CastSpell();
		
		m_anim.enabled = true;
	}
	
	void LavaPattern()
	{
		
	}
	
	void TailSlash()
	{
		//do tail slash animation
	}
	
	void EndBossFight()
	{
		if(groundMoveTime > 0)
		{
			groundMoveTime -= Time.deltaTime;
			ground.transform.Translate(Vector3.up * Time.deltaTime * 10);
		}
		
		if(door != null)
			door.SetActive(false);
	}
	
	IEnumerator Submerge()
	{
		m_anim.SetTrigger("Submerge");
		yield return new WaitForSeconds(1f);
		int rnd = Random.Range (0, positions.Length);
		transform.position = positions[rnd].position;
		SetAllImmune(false);
	}
}
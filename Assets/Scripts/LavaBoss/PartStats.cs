﻿using UnityEngine;
using System.Collections;

public class PartStats : Stats {

	public override void Die()
	{
		LavaBossBehavior boss = transform.root.GetComponent<LavaBossBehavior>();
		boss.SetAllImmune(true);
		isDead = true;
		GetComponent<Renderer>().material.color = Color.gray;
		boss.CheckBossHealth();
	}
}
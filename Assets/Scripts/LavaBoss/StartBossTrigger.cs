﻿using UnityEngine;
using System.Collections;

public class StartBossTrigger : MonoBehaviour {

	public LavaBossBehavior boss;

	void OnTriggerEnter2D(Collider2D col)
	{
		if(col.GetComponent<Movement>())
		{
			boss.StartBossFight();
		}
	}
}
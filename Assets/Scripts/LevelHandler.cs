﻿using UnityEngine;
using System.Collections;

public class LevelHandler : MonoBehaviour {

	public string[] levelNames;
	public int directedLevelID;

	public bool isOnPortal;

	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.name == "Player")
		{
			isOnPortal = true;
			//Application.LoadLevel(levelNames[directedLevelID]);
		}
		else
		{
			isOnPortal = false;
		}
	}

	void OnGUI()
	{
		if (isOnPortal)
		{
			if (GUI.Button (new Rect (100, 100, 150, 50), "Teleport to " + levelNames[directedLevelID]))
			{
				Application.LoadLevel(levelNames[directedLevelID]);
			}
		}
	}
}

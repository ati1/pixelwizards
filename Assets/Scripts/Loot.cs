﻿using UnityEngine;
using System.Collections;

public class Loot : MonoBehaviour {

	private Transform m_transform;
	private int m_amountToDrop;

	public NewPickUp pickUpPrefab;
	public int minAmountOfDrops,maxAmountOfDrops;
	public int goldMax,goldMin;

	[System.Serializable]
	public class DropItem
	{
		public bool empty;
		public string itemName;
		public int amount;
		public bool randomizeAmount;
		public int min,max;
		public float dropChance;
	}
	
	public DropItem[] itemsToDrop;

	public bool dontAddForce;
	private float maximumPropability;
	

	void Start()
	{
		m_transform = transform;
		m_amountToDrop = Random.Range( minAmountOfDrops,maxAmountOfDrops );
		InitItemChances ();
	}
	/// <summary>
	/// Initializes drop chances
	/// </summary>
	void InitItemChances()
	{
		for( int i = 0;i<itemsToDrop.Length;i++ )
		{
			if(i != 0)
			{
				itemsToDrop[i].dropChance += itemsToDrop[i - 1].dropChance;
			}
		}

		maximumPropability = itemsToDrop[itemsToDrop.Length - 1].dropChance;

		for( int i = 0;i<itemsToDrop.Length;i++ )
		{
			int temp = System.Convert.ToInt32(((maximumPropability / 100) * itemsToDrop[i].dropChance));
			itemsToDrop[i].dropChance = (float)temp;
		}

		maximumPropability = itemsToDrop[itemsToDrop.Length - 1].dropChance;
	}
	/// <summary>
	/// Drops the items.
	/// </summary>
	public void DropItems()
	{
		for( int i = 0;i<m_amountToDrop;i++ )
		{
			int rnd = Random.Range(0,(int)maximumPropability + 1);
			DropItem droppedItem = ChooseItem(rnd);
			if( !droppedItem.empty )
			{
				SpawnItem(droppedItem);
			}
		}	
	}
	/// <summary>
	/// Spawns item.
	/// </summary>
	/// <param name="droppedItem">Dropped item.</param>
	public void SpawnItem(DropItem droppedItem)
	{
		NewPickUp temp = GameObject.Instantiate( pickUpPrefab,m_transform.position,Quaternion.identity ) as NewPickUp;
		
		temp.itemName = droppedItem.itemName;
		
		if(droppedItem.randomizeAmount)
			temp.amount = Random.Range(droppedItem.min,droppedItem.max);
		else
			temp.amount = 1;
		
		temp.SetUp();

		if(!dontAddForce)
			temp.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(-1f,1f),1) * 5,ForceMode2D.Impulse);	
	}
	/// <summary>
	/// Choose item to drop by its drop chance
	/// </summary>
	/// <returns>The item.</returns>
	/// <param name="rnd">Random.</param>
	public DropItem ChooseItem(int rnd)
	{
		int maxVal = (int)itemsToDrop [itemsToDrop.Length - 1].dropChance;

		if(rnd == maxVal)
			rnd -= 1;

		int index = 0;
		while (itemsToDrop[index].dropChance <= rnd)
		{
			index++;
		}
		return itemsToDrop [index];
	}
}
﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

public class MenuManager : MonoBehaviour {

	public SaveGame.SaveData[] saveDatas;
	public GameObject buttonHolder;
	public GameObject levelDataButton;
	public GameObject deleteButtonHolder;
	public GameObject deleteButton;
	public SaveGame saveGame;
	public SaveMenu menu;
	public GameObject saveMenuPrefab;
	
	public GameObject options;

    public GameObject MainUI;
    public GameObject loadGame;
    public GameObject backButton;
    public GameObject loginUI;

    public Text logText;
    public GameObject loginForm;
    public GameObject logoutButton;
    public GameObject loggedinUI;

    public GameObject buttonHolderServer;
    public GameObject deleteButtonHolderServer;

    public Text topWizards;

    private bool m_multiplayerWasLoaded;
	
	private List<GameObject> m_addedButtons = new List<GameObject>();
    private List<GameObject> m_addedButtonsServer = new List<GameObject>();

    public static void LoadLevel(string level)
	{
		Application.LoadLevel (level);
	}

	public static void QuitGame()
	{
		Application.Quit ();
	}

	public void Quit()
	{
		Application.Quit ();
	}
	
	public void Multiplayer()
	{
		Application.LoadLevel("Network");
		StartMultiplayer();
	}
	
	public IEnumerator StartMultiplayer()
	{
		while (!m_multiplayerWasLoaded)
		{
			yield return 1;
		}
		m_multiplayerWasLoaded = false;
	
		//init multiplayer
	}
	
	private void OnLevelWasLoaded(int index)
	{
		if(index == 3)
		{
			m_multiplayerWasLoaded = true;
		}
	}

    public void InitSaveDataFromServer()
    {
        //there is wierd bug in the scaling of these buttons ,idk
        foreach (SaveGame.SaveData saveData in SaveDataList.savedGamesServer)
        {
            CreateButtonServer(saveData.name, saveData.name);
        }
    }

    public void CreateButtonServer(string buttonText, string saveIDString)
    {
        GameObject button = (GameObject)GameObject.Instantiate(levelDataButton.gameObject);
        button.GetComponent<RectTransform>().SetParent(buttonHolderServer.transform);
        button.GetComponentInChildren<Text>().text = buttonText;

        button.GetComponent<Button>().onClick.AddListener(() => {

            if (m_addedButtonsServer.Count > 0)
                DeleteButtonsServer();
            menu.LoadGame(saveIDString);

        });

        m_addedButtonsServer.Add(button);

        GameObject deleteSave = (GameObject)GameObject.Instantiate(deleteButton.gameObject);
        deleteSave.GetComponent<RectTransform>().SetParent(deleteButtonHolderServer.transform);

        deleteSave.GetComponent<Button>().onClick.AddListener(() => {

            FindObjectOfType<DBManager>().StartCoroutine(FindObjectOfType<DBManager>().DeleteSaveData(saveIDString));

        });

        m_addedButtonsServer.Add(deleteSave);
    }

    public void DeleteButtonsServer()
    {
        for (int i = 0; i < m_addedButtonsServer.Count; i++)
        {
            Destroy(m_addedButtonsServer[i].gameObject);
        }
        m_addedButtonsServer.Clear();
    }

    public void InitSaveData()
	{
		foreach(SaveGame.SaveData saveData in SaveDataList.savedGames)
		{
			CreateButton(saveData.name,saveData.name/*(SaveDataList.savedGames.IndexOf(saveData) + 1),SaveDataList.savedGames.IndexOf(saveData)*/ );
			//LoadGame(SaveLoad.savedGames.IndexOf(saveData));
		}
	}

	public void CreateButton(string buttonText, string levelName)
	{
		GameObject button = (GameObject)GameObject.Instantiate (levelDataButton.gameObject);
		button.GetComponent<RectTransform>().SetParent(buttonHolder.transform);
		button.GetComponentInChildren<Text> ().text = buttonText;

		button.GetComponent<Button> ().onClick.AddListener(() => { 

			menu.LoadGame(levelName);

		});
		
		m_addedButtons.Add(button);
		
		GameObject deleteSave = (GameObject)GameObject.Instantiate (deleteButton.gameObject);
		deleteSave.GetComponent<RectTransform>().SetParent(deleteButtonHolder.transform);
		
		deleteSave.GetComponent<Button> ().onClick.AddListener(() => { 
			
			menu.DeleteSave(levelName);
			
		});
		
		m_addedButtons.Add(deleteSave);
	}
	
	public void DeleteButtons()
	{
		for( int i = 0;i<m_addedButtons.Count;i++)
		{
			Destroy(m_addedButtons[i].gameObject);
		}
		m_addedButtons.Clear();
	}
	
	public void StartNewGame()
	{
		if(menu.currentSavedGames < menu.maxSavedGames)
		{
			menu.StartNewGame("lobby", menu.input.text);
		}
		else
		{
			menu.logText.text = "You have too many saves!";
		}
	}

    public void ShowLogInTab()
    {
        HideMainUI();
        if(FindObjectOfType<SaveMenu>().session_id == "")
        {
            ShowLogInUI();
            FindObjectOfType<DBManager>().StartCoroutine("GetStatistics");
        }
        else
        {
            //ShowLoggedInUI();
            loadGame.SetActive(false);
            loginUI.SetActive(true);
            loginForm.SetActive(false);
            logoutButton.SetActive(true);
        }
          
    }

    public void ShowLogInUI()
    {
        loadGame.SetActive(false);
        loginUI.SetActive(true);
        loginForm.SetActive(true);
        logoutButton.SetActive(false);
        loggedinUI.SetActive(false);
        logText.text = "Log in or create account!";
        if(m_addedButtonsServer.Count > 0)
            DeleteButtonsServer();
    }

    public void ShowMainUI()
    {
        loadGame.SetActive(true);
        MainUI.SetActive(true);

        foreach (GameObject b in m_addedButtons)
            b.SetActive(true);

        loginUI.SetActive(false);
    }

    public void ShowLoggedInUI(string username)
    {
        loginForm.SetActive(false);
        loggedinUI.SetActive(true);
        logoutButton.SetActive(true);
        logText.text = "Welcome " + username + "!";
    }

    public void HideMainUI()
    {
        MainUI.SetActive(false);

        foreach (GameObject b in m_addedButtons)
            b.SetActive(false);
    }

	public void Init()
	{
		GameObject temp = null;
		if(FindObjectsOfType<SaveMenu>().Length == 0)
		{
			temp =(GameObject)GameObject.Instantiate (saveMenuPrefab);
			saveGame.Init ();
		}
		else
		{
			temp = FindObjectOfType<SaveMenu>().gameObject;
		}
		menu = temp.GetComponent<SaveMenu>();
		saveGame.saveMenu = menu;
		InitSaveData ();
	}

	void Awake()
	{
		Init ();
	}
}
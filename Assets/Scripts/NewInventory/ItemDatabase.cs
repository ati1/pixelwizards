﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemDatabase : MonoBehaviour {

	private ItemFunctions m_itemFunctions;
	public List<string> parameters;

	[System.Serializable]
	public class Item {

		public string itemName = "";

		//public GEventDelegate d;

		public NewInventory owner = null;
		public bool isStackable;
		public int itemID = 0;
		public Sprite img;
		public NewInventory.EquipSlot slot;

		[HideInInspector]
		public GameObject pickUp;
		public int lvlReq = 0;
		public int dmg = 0;
		public int armor = 0;
		public int health = 0;
		public List<Stats.SpellDmg> spellDmg;
		public List<Stats.SpellResistance> spellRes;
		public Sprite icon;

		public string useEffect = "";
		public string itemFunction = "";

		[System.Serializable]
		public class ItemFunctionParameter
		{
			public string value;

			public int getIntValue()
			{
				return System.Convert.ToInt32(value);
			}

			public float getFloatValue()
			{
				return System.Convert.ToSingle(value);
			}

			public string getStringValue()
			{
				return value;
			}

		}
		public List<ItemFunctionParameter> parameters;
		//public NewInventory.RobePiece[] Robe;
		
		[System.Serializable]
		public class Robe
		{
			public Sprite[] piece = new Sprite[10];
			public string[] pieceName = new string[10];
		}
		
		[System.Serializable]
		public class Anim
		{
			public Sprite[] animPieces;
		}
		
		public Robe robe = new Robe();
		
		public bool hasAnim;
		public Anim[] frames;


		
//		public delegate void ItemDelegate();
//		
//		public ItemDelegate UseItem;
		
		public enum Type{
			equipment,
			other
		};
		public enum Rarity{
			common,
			uncommon,
			rare,
			epic
		};
		public Type type;
		public Rarity rarity;
		
		public int maxStack;
		public int currentStack;
		
		public Item()
		{
			
		}
		
		public Item(Item itemCopy)
		{
			owner = itemCopy.owner;
			itemID = itemCopy.itemID;
			isStackable = itemCopy.isStackable;
			img = itemCopy.img;
			slot = itemCopy.slot;
			itemName = itemCopy.itemName;
			
			pickUp = itemCopy.pickUp;
			lvlReq = itemCopy.lvlReq;
			dmg = itemCopy.dmg;
			armor = itemCopy.armor;
			health = itemCopy.health;
			
			List<Stats.SpellDmg> spellDmgList = new List<Stats.SpellDmg> ();
			foreach(Stats.SpellDmg spelldmg in itemCopy.spellDmg)
			{
				spellDmgList.Add(spelldmg);
			}
			spellDmg = spellDmgList;
			
			List<Stats.SpellResistance> spellResiList = new List<Stats.SpellResistance> ();
			foreach(Stats.SpellResistance spellResistance in itemCopy.spellRes)
			{
				spellResiList.Add(spellResistance);
			}
			spellRes = spellResiList;
			
			icon = itemCopy.icon;
			useEffect = itemCopy.useEffect;
			itemFunction = itemCopy.itemFunction;

			List<ItemDatabase.Item.ItemFunctionParameter> m_parameters = new List<ItemDatabase.Item.ItemFunctionParameter>();
			foreach(ItemDatabase.Item.ItemFunctionParameter parameter in itemCopy.parameters)
			{
				m_parameters.Add(parameter);
			}
			parameters = m_parameters;

			type = itemCopy.type;
			rarity = itemCopy.rarity;
			
			maxStack = itemCopy.maxStack;
			currentStack = itemCopy.currentStack;
			
			robe = itemCopy.robe;
			
			hasAnim = itemCopy.hasAnim;
			frames = itemCopy.frames;
			
			
		}
		
		public virtual void InitItem()
		{
			
		}

		public string GetTooltip()
		{
			string returnString = "";
			string splDmgs = "";
			string splResis = "";
			string color = "white";
			
			switch (rarity)
			{
			case Rarity.common:
				color = "white";
				break;
			case Rarity.uncommon:
				color = "green";
				break;
			case Rarity.rare:
				color = "blue";
				break;
			case Rarity.epic:
				color = "magenta";
				break;
			}

			if(spellDmg.Count > 0)
			{
				foreach(Stats.SpellDmg splDmg in spellDmg)
				{
					if(splDmg.dmg != 0)
					{
						splDmgs += splDmg.spellSchool.ToString() + "Dmg " + splDmg.dmg + "\n";
					}
				}
			}

			if(spellRes.Count > 0)
			{
				foreach(Stats.SpellResistance splRes in spellRes)
				{
					if(splRes.amount != 0)
					{
						splDmgs += splRes.spellSchool.ToString() + "Resi " + splRes.amount + "\n";
					}
				}
			}

			string tempUseEffect = "";
			if(useEffect != "")
			{
				tempUseEffect += useEffect;
			}
			
			if(type == Type.equipment)
			{
				returnString += "<color=" +color+">"+itemName +"</Color> \n" + "requires lvl " + lvlReq + "\n" + type.ToString () + "\n" +
					slot.ToString () + "\n" + "BonusHP " + health + "\n" +
						"SpellDmg " + dmg.ToString () + "\n" +
						"Armor " + armor.ToString () + "\n" + 
						splDmgs + "\n" + splResis + "<color=green>" + tempUseEffect + "</color>";
			}
			else if(type == Type.other)
			{
				returnString += "<color=" +color+">"+itemName +"</Color> \n" + "requires lvl " + lvlReq + "\n" + type.ToString () + "\n" +
					slot.ToString () + " \n<color=green>" + tempUseEffect + "</color>\n";
			}
			
			return returnString;
		}
	}

	public List<Item> allItems;
	public static List<Item> items;

	void Awake()
	{
		m_itemFunctions = GetComponent<ItemFunctions>();
		InitItems ();
	}

	void InitItems()
	{
		items = allItems;
	}

	public void UseItem(Slot slot)
	{
		ItemFunctions.ItemDelegate d;
		m_itemFunctions.itemFunctions.TryGetValue(slot.item.itemFunction, out d);

		if(d != null)
		{
			switch(slot.item.itemFunction)
			{
			case "Heal":
				Stats stats = slot.item.owner.GetComponent<Stats>();
				float upcomingHeal = (slot.item.parameters[0].getFloatValue() / 100) * stats.maxHealth;

				d(slot,stats,upcomingHeal);
				break;
			}
		}
	}
	
	public static Item GetItemByName(string itemName)
	{
		for( int i = 0;i<ItemDatabase.items.Count;i++ )
		{
			if(ItemDatabase.items[i].itemName == itemName)
			{
				return ItemDatabase.items[i];
			}
		 }
		 return null;	   
	}

	public static Item GetItemById(int id)
	{
		for( int i = 0;i<ItemDatabase.items.Count;i++ )
		{
			if(ItemDatabase.items[i].itemID == id)
			{
				return ItemDatabase.items[i];
			}
		}
		return null;	   
	}
}
﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class NewInventory : MonoBehaviour {

	public SpriteRenderer hat, weaponSlot;
	//public List<SpriteRenderer>robe;
	
	[System.Serializable]
	public class RobePiece
	{
		[SerializeField]
		public SpriteRenderer piece;
		[SerializeField]
		public string pieceName;
	}
	
	public RobePiece[] robe = new RobePiece[10];
	public Sprite[] defaultRobe = new Sprite[10];
	
	public Vector2 mousePos;
	public enum EquipSlot
	{
		head,
		robe,
		neck,
		ring,
		trinket,
		weapon,
		none
	};
	public List<Slot> equippedItems = new List<Slot>();
	public List<Slot> inventorySlots = new List<Slot>();
	public Text tooltipText;
	public Text statsText;
	private Stats m_stats;
	
	public bool multiPlayer;

	void Awake()
	{
		if(multiPlayer)
		{
			tooltipText = GameObject.Find("tooltipText").GetComponent<Text>();
			statsText = GameObject.Find("statsText").GetComponent<Text>();
		
			InitInventory ();
			m_stats = GetComponent<Stats> ();
		}
		else
		{
			InitInventory ();
			m_stats = GetComponent<Stats> ();
		}
	}

	void Start()
	{
		UpdateStatText ();
	}

	void InitInventory()
	{
		Slot[] slots = FindObjectsOfType<Slot> ();
		foreach(Slot slot in slots)
		{
			if(slot.slotType == Slot.SlotType.Equip)
			{
				equippedItems.Add(slot);
			}
			else if (slot.slotType == Slot.SlotType.Inventory)
			{
				inventorySlots.Add(slot);
			}
		}
	}

	public void SetSprite(ItemDatabase.Item item)
	{
		switch(item.slot)
		{
		case EquipSlot.head:
			if(item.hasAnim)
			{
				LegacySpriteAnim anim = hat.GetComponent<LegacySpriteAnim>();
				anim.sprites = item.frames[0].animPieces;
				anim.enabled = true;
			}
			hat.sprite = item.img;
			break;
		case EquipSlot.robe:
			if(item.hasAnim)
			{
				for(int i = 0;i<item.frames.Length;i++)
				{
					LegacySpriteAnim anim = robe[i].piece.GetComponent<LegacySpriteAnim>();
					if(anim != null)
					{
						anim.sprites = item.frames[i].animPieces;
						anim.enabled = true;
					}
				}
			}
			for(int i = 0;i<robe.Length;i++)
			{
				for(int j = 0;j<item.robe.pieceName.Length;j++)
				{
					if( string.Equals(robe[i].pieceName, item.robe.pieceName[j], System.StringComparison.OrdinalIgnoreCase) )
					{
						robe[i].piece.sprite = item.robe.piece[j];
					}
				}
			}
			break;
		case EquipSlot.weapon:
			if(item.hasAnim)
			{
				LegacySpriteAnim anim = weaponSlot.GetComponent<LegacySpriteAnim>();
				anim.sprites = item.frames[0].animPieces;
				anim.enabled = true;
			}
			weaponSlot.sprite = item.img;
			break;
		}
	}

	public void RemoveSprite(ItemDatabase.Item item)
	{
		switch(item.slot)
		{
		case EquipSlot.head:
			if(item.hasAnim)
			{
				LegacySpriteAnim anim = hat.GetComponent<LegacySpriteAnim>();
				anim.sprites = new Sprite[0];
				anim.enabled = false;
			}
			hat.sprite = null;
			break;
		case EquipSlot.robe:
			//set basic sprites back
			if(item.hasAnim)
			{
				for(int i = 0;i<item.frames.Length;i++)
				{
					LegacySpriteAnim anim = robe[i].piece.GetComponent<LegacySpriteAnim>();
					if(anim != null)
					{
						anim.sprites = new Sprite[0];
						anim.enabled = false;
					}
				}
			}
			for(int i = 0;i<robe.Length;i++)
			{
				robe[i].piece.sprite = defaultRobe[i];
			}
			break;
		case EquipSlot.weapon:
			if(item.hasAnim)
			{
				LegacySpriteAnim anim = weaponSlot.GetComponent<LegacySpriteAnim>();
				anim.sprites = new Sprite[0];
				anim.enabled = false;
			}
			weaponSlot.sprite = null;
			break;
		}
	}

	public bool AddItem(ItemDatabase.Item item)
	{
		if(item != null)
		{
			if(item.isStackable)
			{
				foreach(Slot slot in inventorySlots)
				{
					if(slot.item != null && slot.item.itemName == item.itemName && slot.hasItem)
					{
						if(slot.currentStack < slot.maxStack)
						{
							slot.currentStack++;
							slot.UpdateStackText();
							return true;
						}
					}
				}
			}
			
			foreach(Slot slot in inventorySlots)
			{
				if(!slot.hasItem)
				{
					slot.SetItem(item);
					return true;
				}
			}
		}
		return false;
	}
	
	public void RemoveItemFromInventory(int index)
	{
		for( int i = 0;i<inventorySlots.Count;i++ )
		{
			if(i == index)
			{
				inventorySlots[i].EmptySlot();
			}
		}
	}
	
	public void MoveItem(ItemDatabase.Item item, Slot moveFrom, Slot moveTo)
	{
		if(item != null)
		{
			//move inside inventory
			if(moveFrom.slotType == Slot.SlotType.Inventory && moveTo.slotType == Slot.SlotType.Inventory)
			{
				if(!moveTo.hasItem && item != null)
				{
					moveTo.SetItem(item);
					moveFrom.EmptySlot();
				}
				//swap items
				else
				{
					ItemDatabase.Item temp = moveTo.item;
					ItemDatabase.Item temp2 = moveFrom.item;
					moveTo.EmptySlot();
					moveFrom.EmptySlot();
					moveTo.SetItem(temp2);
					moveFrom.SetItem(temp);
				}
			}
			//move from equipped slot to inventory
			if(moveFrom.slotType == Slot.SlotType.Equip && moveTo.slotType == Slot.SlotType.Inventory)
			{
				if(!moveTo.hasItem)
				{
					m_stats.RemoveItemStats(item);
					UpdateStatText();
					moveTo.SetItem(item);
					RemoveSprite(item);
					moveFrom.EmptySlot();
				}
				else
				{
					//check if we are swapping item with one in inventory and we have high enought level to use it
					if(moveTo.item.slot == item.slot)
					{
						if(moveTo.item.lvlReq <= m_stats.exp.GetCurrentLvl())
						{
							ItemDatabase.Item temp = moveTo.item;
							ItemDatabase.Item temp2 = moveFrom.item;
							if(EquipItem(moveTo.item))
							{
								m_stats.RemoveItemStats(item);
								m_stats.GetItemStats(moveTo.item);

								moveTo.EmptySlot();
								moveFrom.EmptySlot();

								RemoveSprite(temp2);
								SetSprite(temp);

								moveTo.SetItem(temp2);
								moveFrom.SetItem(temp);
								UpdateStatText();
							}
						}
					}

				}
			}
			//equip item
			if(moveFrom.slotType == Slot.SlotType.Inventory && moveTo.slotType == Slot.SlotType.Equip)
			{
				if(!moveTo.hasItem)
				{
					if(moveTo.equipSlot == moveFrom.item.slot)
					{
						if(EquipItem(item))
						{
							m_stats.GetItemStats(item);
							SetSprite(item);
							UpdateStatText();
							moveFrom.EmptySlot();
						}
					}
				}
				//equip item and de equip current item
				else
				{
					if(moveFrom.item.lvlReq <= m_stats.exp.GetCurrentLvl())
					{
						ItemDatabase.Item temp = moveTo.item;
						ItemDatabase.Item temp2 = moveFrom.item;

						moveTo.EmptySlot();
						moveFrom.EmptySlot();

						m_stats.RemoveItemStats(temp);
						m_stats.GetItemStats(temp2);

						RemoveSprite(temp);
						SetSprite(temp2);

						moveTo.SetItem(temp2);
						moveFrom.SetItem(temp);

						UpdateStatText();
					}
				}
			}
		}
	}
	public bool EquipItem(ItemDatabase.Item item)
	{
		bool returnValue = false;
		if(item != null)
		{
			if(item.type == ItemDatabase.Item.Type.equipment)
			{
				foreach(Slot slot in equippedItems)
				{
					if(slot.equipSlot == item.slot && m_stats.exp.GetCurrentLvl() >= item.lvlReq)
					{
						slot.SetItem(item);
						SetSprite(item);
						returnValue = true;
					}
				}
			}
			else
			{
				Debug.Log("Cant equip that item");
			}
		}
		return returnValue;
	}
	public bool IsFull(ItemDatabase.Item itemToAdd)
	{
		if(itemToAdd != null)
		{
			foreach(Slot slot in inventorySlots)
			{
				if(slot.item != null && slot.item.itemName == itemToAdd.itemName && slot.currentStack < slot.maxStack || !slot.hasItem )
				{
					return false;
				}
			}
		}
		return true;
	}
	public void UpdateStatText()
	{
		if(m_stats != null)
			statsText.text = m_stats.GetStatsString ();
	}
}
﻿using UnityEngine;
using System.Collections;

public class NewPickUp : MonoBehaviour {

	public string itemName;
	public int amount;


	void Start()
	{
		SetUp();
	}

	public void SetUp()
	{
		foreach(ItemDatabase.Item item in ItemDatabase.items)
		{
			if(itemName == item.itemName)
				GetComponent<SpriteRenderer>().sprite = item.img;
		}
	}
	
	void OnTriggerStay2D(Collider2D col)
	{
		if (col.GetComponent<NewInventory> ())
		{
			NewInventory inv = col.GetComponent<NewInventory> ();
			foreach(ItemDatabase.Item item in ItemDatabase.items)
			{
				if(item.itemName == itemName)
				{
					ItemDatabase.Item newItem = new ItemDatabase.Item(item);
					for(int i = 0;i<amount;i++)
					{
						if(!inv.IsFull(newItem))
						{
							newItem.owner = inv;
							inv.AddItem(newItem);
							amount--;
						}
					}
					if(amount == 0)
					{
						Destroy(gameObject);
					}
				}
			}
		}
	}
	int GetRandomAmount()
	{
		return Random.Range (0, amount);
	}
}
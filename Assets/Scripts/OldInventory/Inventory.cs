﻿//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//
//public class Inventory : MonoBehaviour {
//
//	[System.Serializable]
//	public class Money
//	{
//		public int amount = 0;
//		
//		public void AddMoney(int addedAmount)
//		{
//			amount += addedAmount;
//		}
//		public void RemoveMoney(int removedAmount)
//		{
//			amount -= removedAmount;
//		}
//	}
//	public SpriteRenderer hat;
//	public List<SpriteRenderer> robe;
//	public SpriteRenderer weaponSlot;
//
//	public enum EquipSlot
//	{
//		head,
//		robe,
//		neck,
//		ring,
//		trinket,
//		weapon,
//		none
//	};
//	[System.Serializable]
//	public class ItemSlot
//	{
//		public EquipSlot slot;
//		public bool hasItem = false;
//		public Item equippedItem;
//
//	}
//	public List<ItemSlot> equippedItems = new List<ItemSlot>{
//		new ItemSlot{slot = EquipSlot.head},
//		new ItemSlot{slot = EquipSlot.neck},
//		new ItemSlot{slot = EquipSlot.ring},
//		new ItemSlot{slot = EquipSlot.trinket},
//		new ItemSlot{slot = EquipSlot.robe},
//		new ItemSlot{slot = EquipSlot.weapon},
//	};
//
//	[System.Serializable]
//	public class InventorySlot
//	{
//		public int maxStack, currentStack;
//		public Item item;
//	}
//	public Money money;
//	public List<Item> inventory;
//	public List<InventorySlot> inventorySlots;
//	public Stats stats;
//
//	public Rect GUIAreaRect;
//
//	public int horizontalAmount = 4;
//
//	public bool showInventory;
//
//	private int horizontalIndex = 0;
//
//	private bool itemIsSelected = false;
//
//	private Item selectedItem = null;
//
//	void Awake()
//	{
//		if(GetComponent<Stats>())
//			stats = GetComponent<Stats>();
//	}
//
//	void Update () 
//	{
//		ShowInventory ();
//		if(Input.GetKeyDown(KeyCode.Mouse0) && !GUIAreaRect.Contains(GetMousePosInGUI()) && showInventory || !showInventory)
//		{
//			DeSelectItem();
//		}
//	}
//	Vector2 GetMousePosInGUI()
//	{
//		return new Vector2 (Input.mousePosition.x, Screen.height - Input.mousePosition.y);
//	}
//	void ShowInventory()
//	{
//		if(Input.GetKeyDown(KeyCode.C) && showInventory == false)
//		{
//			showInventory = true;
//		}
//		else if(Input.GetKeyDown(KeyCode.C) && showInventory == true)
//		{
//			showInventory = false;
//		}
//	}
//	void SelectItem(Item item)
//	{
//		itemIsSelected = true;
//		selectedItem = item;
//	}
//	void DeSelectItem()
//	{
//		selectedItem = null;
//		itemIsSelected = false;
//	}
//	void MoveSelectedItem(int moveToIndex)
//	{
//		if(!inventory.Contains(selectedItem))
//		{
//			int index = 0;
//			foreach(ItemSlot slot in equippedItems)
//			{
//				if(slot.equippedItem != null)
//				{
//					if(slot.equippedItem.slot == selectedItem.slot)
//					{
//						index = equippedItems.IndexOf(slot);
//					}
//				}
//			}
//			if(inventory[moveToIndex] != null)
//			{
//				if(inventory[moveToIndex].slot == selectedItem.slot)
//				{
//					stats.RemoveItemStats(equippedItems[index].equippedItem);
//					RemoveSprite(equippedItems[index].equippedItem);
//					equippedItems[index].equippedItem = inventory[moveToIndex];
//					inventory[moveToIndex] = selectedItem;
//				}
//			}
//			else
//			{
//				stats.RemoveItemStats(equippedItems[index].equippedItem);
//				RemoveSprite(equippedItems[index].equippedItem);
//				equippedItems[index].equippedItem = null;
//				equippedItems[index].hasItem = false;
//				inventory[moveToIndex] = selectedItem;
//			}
//		}
//		else
//		{
//			if(inventory[moveToIndex] != null)
//			{
//				inventory[inventory.IndexOf (selectedItem)] = inventory[moveToIndex];
//				inventory[moveToIndex] = selectedItem;
//			}
//			else
//			{
//				inventory[inventory.IndexOf(selectedItem)] = null;
//				inventory[moveToIndex] = selectedItem;
//			}
//		}
//
//		DeSelectItem();
//	}
//
//	public bool AddItem(Item item)
//	{
//		bool value = false;
//		if(item.type == Item.Type.equipment)
//		{
//			for(int i = 0;i<inventory.Count;i++)
//			{
//				if(inventory[i] == null)
//				{
//					inventory[i] = item;
//					value = true;
//					break;
//				}
//			}
//		}
//		else if(item.type == Item.Type.other)
//		{
//			for(int i = 0;i<inventory.Count;i++)
//			{
//				if(inventory[i] != null && inventory[i].itemName == item.itemName)
//				{
//					if(item.currentStack < item.maxStack)
//					{
//						item.currentStack++;
//						value = true;
//						break;
//					}
//				}
//			}
//			if(!value)
//			{
//				for(int i = 0;i<inventory.Count;i++)
//				{
//					if(inventory[i] == null)
//					{
//						inventory[i] = item;
//						value = true;
//						break;
//					}
//				}
//			}
//		}
//		return value;
//	}
//	public void RemoveItemFromInventory(int indexOfItem)
//	{
//		inventory [indexOfItem] = null;
//	}
//	void EquipItem(Item item)
//	{
//		foreach(ItemSlot slot in equippedItems)
//		{
//			if(slot.slot == item.slot && item.lvlReq <= stats.exp.currentLvl)
//			{
//				SetSprite(item);
//				stats.RemoveItemStats(slot.equippedItem);
//				slot.equippedItem = item;
//				slot.hasItem = true;
//				inventory[inventory.IndexOf(item)] = null;
//				//stats.GetItemStats(item);
//			}
//		}
//	}
//	void SetSprite(Item item)
//	{
//		switch(item.slot)
//		{
//		case EquipSlot.head:
//			hat.sprite = item.img;
//			break;
//		case EquipSlot.robe:
//			for(int i = 0;i<robe.Count;i++)
//			{
//				robe[i].sprite = item.RobeSprites[i];
//			}
//			break;
//		case EquipSlot.weapon:
//			weaponSlot.sprite = item.img;
//			break;
//		}
//	}
//	void RemoveSprite(Item item)
//	{
//		switch(item.slot)
//		{
//		case EquipSlot.head:
//			hat.sprite = null;
//			break;
//		case EquipSlot.robe:
//			 //set basic sprites back
//			break;
//		case EquipSlot.weapon:
//			weaponSlot.sprite = null;
//			break;
//		}
//	}
//	public void DeEquipItem(ItemSlot removeSlot)
//	{
//		foreach(ItemSlot slot in equippedItems)
//		{
//			if(slot.slot == removeSlot.slot)
//			{
//				stats.RemoveItemStats(slot.equippedItem);
//				if(AddItem(slot.equippedItem))
//				{
//					RemoveSprite(slot.equippedItem);
//					slot.equippedItem = null;
//					slot.hasItem = false;
//				}
//				else
//				{
//					Debug.Log("Inventory is full");
//				}
//			}
//		}
//	}
//	void OnGUI()
//	{
//		if(showInventory)
//		{
//			GUI.Box(new Rect(200,125,750,350),"");
//			if(GUI.Button(new Rect(100,100,100,25),"Add 5000 exp"))
//			{
//				stats.AddExp(5000);
//			}
//			GUILayout.BeginArea(GUIAreaRect);
//			GUILayout.BeginHorizontal();
//
//			for(int z = 0;z<equippedItems.Count;z++)
//			{
//				if(equippedItems[z].equippedItem != null)
//				{
//					if(GUILayout.Button(new GUIContent(equippedItems[z].equippedItem.icon,GetTooltipFromItem(equippedItems[z].equippedItem)),GUILayout.Width(50),GUILayout.Height(50)))
//					{
//						if(Input.GetKeyUp(KeyCode.Mouse1))
//						{
//							DeEquipItem(equippedItems[z]);
//						}
//						else if(Input.GetKeyUp(KeyCode.Mouse0) && !itemIsSelected)
//						{
//							SelectItem(equippedItems[z].equippedItem);
//						}
//					}
//				}
//				else
//				{
//					if(GUILayout.Button(equippedItems[z].slot.ToString(),GUILayout.Width(55),GUILayout.Height(50)) && itemIsSelected)
//					{
//						if(equippedItems[z].slot == selectedItem.slot)
//						{
//							EquipItem(selectedItem);
//							DeSelectItem();
//						}
//					}
//				}
//			}
//			GUILayout.Space(20);
//
//
//			GUI.Box(new Rect(400,0,200,310),GetStatsString(stats));
//			
//			GUILayout.EndHorizontal();
//			GUILayout.Space(15);
//			GUILayout.BeginHorizontal();
//
//			bool temp = false;
//			horizontalIndex = 0;
//			for(int i = 0;i<inventory.Count;i++)
//			{
//				if(temp == false)
//				{
//					temp = true;
//					GUILayout.BeginVertical();
//					GUILayout.BeginHorizontal();
//				}
//				if(horizontalIndex==horizontalAmount)
//				{
//					horizontalIndex = 0;
//					GUILayout.EndHorizontal();
//					GUILayout.BeginHorizontal();
//				}
//				if(i == inventory.Count)
//				{
//					horizontalIndex = 0;
//					GUILayout.EndHorizontal();
//					GUILayout.EndVertical();
//				}
//				if(horizontalIndex != horizontalAmount)
//				{
//					if(inventory[i] != null)
//					{
//						if(GUILayout.Button(new GUIContent(inventory[i].icon,GetTooltipFromItem(inventory[i])),GUILayout.Width(35),GUILayout.Height(35)))
//						{
//							if(Input.GetKeyUp(KeyCode.Mouse1))
//							{
//								switch(inventory[i].type)
//								{
//								case Item.Type.equipment:
//									EquipItem(inventory[i]);
//									break;
//								case Item.Type.other:
//									inventory[i].InitItem();
//									inventory[i].UseItem();
//									if(inventory[i].currentStack == 0)
//									{
//										inventory[i] = null;
//									}
//									break;
//								}
//							}
//							else if(Input.GetKeyUp(KeyCode.Mouse0) && inventory[i] != null && !itemIsSelected)
//							{
//								SelectItem(inventory[i]);
//							}
//							else if(Input.GetKeyUp(KeyCode.Mouse0) && inventory[i] != null && itemIsSelected)
//							{
//								MoveSelectedItem(i);
//							}
//						}
//					}
//					else
//					{
//						if(GUILayout.Button("",GUILayout.Width(35),GUILayout.Height(35)))
//						{
//							if(Input.GetKeyUp(KeyCode.Mouse0) && inventory[i] == null && itemIsSelected)
//							{
//								MoveSelectedItem(i);
//							}
//						}
//					}
//				}
//				horizontalIndex++;
//			}
//			GUILayout.Space(10);
//
//			GUI.Label(new Rect(200,75,200,250),GUI.tooltip);
//			GUILayout.EndHorizontal();
//			GUILayout.EndArea();
//
//			if(itemIsSelected)
//			{
//				if(selectedItem.icon != null)
//				{
//					GUI.DrawTexture(new Rect(GetMousePosInGUI().x,GetMousePosInGUI().y,25,25),selectedItem.icon);
//				}
//			}
//		}
//	}
//	string GetStatsString(Stats stats)
//	{
//		string returnString = "";
//		string tempSplDmgs = "";
//		string TemosplResis = "";
//		foreach(Stats.SpellDmg splDmg in stats.spellDmgs)
//		{
//			tempSplDmgs += splDmg.spellSchool.ToString() + "Dmg " + splDmg.dmg + "\n";
//		}
//		foreach(Stats.SpellResistance splRes in stats.spellResistances)
//		{
//			TemosplResis += splRes.spellSchool.ToString() + "Resi " + splRes.amount + "\n";
//		}
//		returnString += "Stats \n" + "Lvl " + stats.exp.GetCurrentLvl().ToString() + "\n" + "Health " + stats.health + "/" + stats.maxHealth + "\n" + "Armor " + stats.armor + "\n" +
//						"SpellDmg " + stats.dmg + "\n\n" + tempSplDmgs + "\n" + TemosplResis + "\n" + "ExpToNextLvl " + stats.exp.GetExpToNextLvl().ToString() + 
//				"\n" + "TotalExp " + stats.exp.totalExpEarned.ToString();
//		return returnString;
//	}
//	string GetTooltipFromItem(Item item)
//	{
//		string returnString = "";
//		string splDmgs = "";
//		string splResis = "";
//		string color = "white";
//
//		switch (item.rarity)
//		{
//		case Item.Rarity.common:
//			color = "white";
//			break;
//		case Item.Rarity.uncommon:
//			color = "green";
//			break;
//		case Item.Rarity.rare:
//			color = "blue";
//			break;
//		case Item.Rarity.epic:
//			color = "magenta";
//			break;
//		}
//
//		foreach(Stats.SpellDmg splDmg in item.spellDmg)
//		{
//			if(splDmg.dmg != 0)
//			{
//				splDmgs += splDmg.spellSchool.ToString() + "Dmg " + splDmg.dmg + "\n";
//			}
//		}
//		foreach(Stats.SpellResistance splRes in item.spellRes)
//		{
//			if(splRes.amount != 0)
//			{
//				splDmgs += splRes.spellSchool.ToString() + "Resi " + splRes.amount + "\n";
//			}
//		}
//
//		string useEffect = "";
//		if(item.useEffect != "")
//		{
//			useEffect += "On use: " +item.useEffect;
//		}
//
//		if(item.type == Item.Type.equipment)
//		{
//			returnString += "<color=" +color+">"+item.itemName +"</Color> \n" + "requires lvl " + item.lvlReq + "\n" + item.type.ToString () + "\n" +
//							item.slot.ToString () + "\n" + "BonusHP " + item.health + "\n" +
//							"SpellDmg " + item.dmg.ToString () + "\n" +
//							"Armor " + item.armor.ToString () + "\n" + 
//							splDmgs + "\n" + splResis + "<color=green>" + useEffect + "</color>";
//		}
//		else if(item.type == Item.Type.other)
//		{
//			returnString += "<color=" +color+">"+item.itemName +"</Color> \n" + "requires lvl " + item.lvlReq + "\n" + item.type.ToString () + "\n" +
//				item.slot.ToString () + " \n<color=green>" + useEffect + "</color>\n" + item.currentStack + "/" + item.maxStack;
//		}
//
//		return returnString;
//	}
//}
﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Item : MonoBehaviour{

	public NewInventory owner = null;
	public bool isStackAble;
	public int itemID = 0;
	public Sprite img;
	public NewInventory.EquipSlot slot;
	public string itemName = "";
	[HideInInspector]
	public GameObject pickUp;
	public int lvlReq = 0;
	public int dmg = 0;
	public int armor = 0;
	public int health = 0;
	public List<Stats.SpellDmg> spellDmg;
	public List<Stats.SpellResistance> spellRes;
	public Texture2D icon;
	public string useEffect = "";

	public List<Sprite> RobeSprites;
	
	public delegate void ItemDelegate();

	public ItemDelegate UseItem;

	public enum Type{
		equipment,
		other
	};
	public enum Rarity{
		common,
		uncommon,
		rare,
		epic
	};
	public Type type;
	public Rarity rarity;

	public int maxStack;
	public int currentStack;

	public Item()
	{

	}

	public Item CopyItem(Item itemCopy)
	{
		Item itemToCopy = new Item ();
		itemToCopy.owner = itemCopy.owner;
		itemToCopy.itemID = itemCopy.itemID;
		itemToCopy.img = itemCopy.img;
		itemToCopy.slot = itemCopy.slot;
		itemToCopy.itemName = itemCopy.itemName;

		itemToCopy.pickUp = itemCopy.pickUp;
		itemToCopy.lvlReq = itemCopy.lvlReq;
		itemToCopy.dmg = itemCopy.dmg;
		itemToCopy.armor = itemCopy.armor;
		itemToCopy.health = itemCopy.health;

		List<Stats.SpellDmg> spellDmgList = new List<Stats.SpellDmg> ();
		foreach(Stats.SpellDmg spelldmg in itemCopy.spellDmg)
		{
			spellDmgList.Add(spelldmg);
		}
		itemToCopy.spellDmg = spellDmgList;

		List<Stats.SpellResistance> spellResiList = new List<Stats.SpellResistance> ();
		foreach(Stats.SpellResistance spellResistance in itemCopy.spellRes)
		{
			spellResiList.Add(spellResistance);
		}
		itemToCopy.spellRes = spellResiList;

		itemToCopy.icon = itemCopy.icon;
		itemToCopy.useEffect = itemCopy.useEffect;
		itemToCopy.type = itemCopy.type;
		itemToCopy.rarity = itemCopy.rarity;

		itemToCopy.maxStack = itemCopy.maxStack;
		itemToCopy.currentStack = itemCopy.currentStack;

		return itemToCopy;

	}

	public virtual void InitItem()
	{

	}
}
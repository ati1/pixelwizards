﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ActiveSpells : MonoBehaviour {

	public GULegacyGUI buttonAreaPos;

	public bool canCast = true;
	public bool castSpell = false;

	public float globalCd = 0.1f;
	private float cd = 0;
	private bool canCastGlobal = false;

	[System.Serializable]
	public class ActionButton
	{
		public string text = "";
		public int buttonID = 0;
		public KeyCode keybind;
		public Spell spell;
		public Image img;

		public float cooldownTimer = 0;
		public bool canCast = false;
		public Text panelText;

	}
	public Transform aim;
	public List<ActionButton> actionButtons;

	public GameObject InventoryUI;
	public GameObject TalentUI;
	public GameObject MenuUI;
	
	public void CastSpell(int id)
	{
		foreach(ActionButton ab in actionButtons)
		{
			if(ab.buttonID == id)
			{
				if(ab.canCast)
				{
					ab.canCast = false;
					ab.cooldownTimer = ab.spell.cooldown;
					castSpell = true;
					if(Network.isClient || Network.isServer)
						GetComponent<NetworkView>().RPC ("UseSpell", RPCMode.All, ab.spell.spellName, aim.position, Quaternion.Euler (ab.spell.transform.eulerAngles.x,
						                                                                                              ab.spell.transform.eulerAngles.y,
						                                                                                              aim.eulerAngles.z));
					else
					{
						GameObject spell = GameObject.Instantiate(ab.spell.gameObject,aim.position,Quaternion.Euler(ab.spell.transform.eulerAngles.x,
							                                                                                            ab.spell.transform.eulerAngles.y,
							                                                                                            aim.eulerAngles.z)) as GameObject;
						Spell temp = spell.GetComponent<Spell>();
						temp.shooter = GetComponent<Stats>();

						temp.SetDmg();
						temp.CastSpell();
					}
				}
			}
		}
	}

	void Update () 
	{
		if(cd <= 0)
		{
			cd = 0;
			canCastGlobal = true;
		}
		foreach(ActionButton ab in actionButtons)
		{
			if(Input.GetKeyDown(ab.keybind))
			{
				if(InventoryUI.activeInHierarchy || TalentUI.activeInHierarchy || MenuUI.activeInHierarchy)
					canCast = false;
				else
					canCast = true;

				if(canCastGlobal && ab.spell != null && canCast)
				{
					CastSpell(ab.buttonID);

					canCastGlobal = false;
					cd = globalCd;
				}
			}
			if(ab.cooldownTimer <= 0)
			{
				ab.cooldownTimer = 0;
				ab.canCast = true;
			}
			if(ab.cooldownTimer != 0)
				ab.cooldownTimer -= Time.deltaTime;
		}
		if(cd != 0)
			cd -= Time.deltaTime;

		UpdateSpellIconText();
	}
	//------------ NEW UI STUFF ------------------------------//
	void UpdateSpellIconText()
	{
		foreach(ActionButton ab in actionButtons)
		{
			if(ab.panelText != null)
			{
				if(!ab.canCast)
					ab.panelText.text = ab.cooldownTimer.ToString("F1");
				else
					ab.panelText.text = "";
			}
		}
	}
//	void OnGUI()
//	{
//		if(Network.isServer || Network.isClient)
//		{
//			if(networkView.isMine)
//			{
//				GUIStyle centeredStyle = GUI.skin.GetStyle("Box");
//				centeredStyle.alignment = TextAnchor.MiddleCenter;
//				GUILayout.BeginArea(new Rect(buttonAreaPos.GetPosition(20,450).x,buttonAreaPos.GetPosition(20,450).y,1000,50));
//				GUILayout.BeginHorizontal();
//				foreach(ActionButton ab in actionButtons)
//				{
//					string temp = "";
//					
//					if(ab.canCast == false)
//						temp = ab.cooldownTimer.ToString("F1");
//					else
//						temp = ab.text;
//					
//					GUILayout.Box(temp,centeredStyle,GUILayout.Width(100),GUILayout.Height(50));
//				}
//			}
//		}
//		else
//		{
//			GUIStyle centeredStyle = GUI.skin.GetStyle("Box");
//			centeredStyle.alignment = TextAnchor.MiddleCenter;
//			GUILayout.BeginArea(new Rect(buttonAreaPos.GetPosition(20,450).x,buttonAreaPos.GetPosition(20,450).y,1000,50));
//			GUILayout.BeginHorizontal();
//			foreach(ActionButton ab in actionButtons)
//			{
//				string temp = "";
//					
//				if(ab.canCast == false)
//					temp = ab.cooldownTimer.ToString("F1");
//				else
//					temp = ab.text;
//					
//				GUILayout.Box(temp,centeredStyle,GUILayout.Width(100),GUILayout.Height(50));
//			}
//		}
//		GUILayout.EndHorizontal();
//		GUILayout.EndArea();
//	}
}
﻿using UnityEngine;
using System.Collections;

public class Aim : MonoBehaviour {

	private Vector2 m_mousePos;
	private Transform m_transform;

	private Vector3 pos;
	private Vector3 dir;
	private float angle;

	void Start () 
	{
		m_transform = transform;
	}
	void Update () 
	{
		pos = Camera.main.WorldToScreenPoint(m_transform.position);
		dir = Input.mousePosition - pos;
		angle = Mathf.Atan2(-dir.y, -dir.x) * Mathf.Rad2Deg;
		m_transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
	}
	public float GetHorizontalAimDirectionNormalized()
	{
		return dir.normalized.x;
	}
}
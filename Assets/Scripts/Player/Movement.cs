﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Movement : MonoBehaviour {

	//public GULegacyGUI levitateGuiPos;

	public bool allowMovement = true;
	private Vector2 m_moveDirection = Vector2.zero;
	public float runSpeed = 10;
	public float speed = 5;
	public float yVelocity = 0;
	public float fallingSpeed = 10;
	public float jumpSpeed = 0;

	private float levitateTime = 0;
	public float maxLevitateTime = 5;
	private bool grounded = false;

	private Rigidbody2D m_rigidbody;

	private Transform m_transform;

	public LayerMask layer;

	private bool allowJump;
	private bool isJumping;
	public ParticleSystem jumpParticle;
	
	public Image levitateEnergyBar;
	private RectTransform m_rect;
	private float m_levitateBarlenght;

	public bool groundedLastFrame;
	public Transform movingPlatform;

	public bool Grounded()
	{
		return grounded;
	}

	public bool IsLevitating()
	{
		return isJumping;
	}
	public float GetCurrentSpeed()
	{
		return m_rigidbody.velocity.magnitude;
	}

	public float GetDirection()
	{
		return Input.GetAxis ("Horizontal");
	}

	private Vector2 dist;

	void Awake () 
	{
		m_rigidbody = GetComponent<Rigidbody2D> ();
		m_transform = transform;
		levitateTime = maxLevitateTime;
		m_rect = levitateEnergyBar.GetComponent<RectTransform>();

	}

	void Update()
	{
		DoTimer();
		AdjustLevitateEnergyBar();
	}

	void FixedUpdate () 
	{
		if(allowMovement)
			DoMovement();
	}
	//---------------------------New UI stuff---------------------------------------------//
	void AdjustLevitateEnergyBar()
	{
		// DO NOT TOUCH
		m_levitateBarlenght = ((levitateTime / maxLevitateTime) + (-1f)) * 227f;
		m_rect.sizeDelta = new Vector2(m_levitateBarlenght,m_rect.sizeDelta.y);
	}

//	void OnGUI()
//	{
//		float length = levitateTime * 100; 
//		levitateTextRect.width = length;
//		GUI.Box(levitateGuiPos.GetPosition(length,25),"");
//	}
	void DoTimer()
	{
		if(levitateTime >= maxLevitateTime)
		{
			levitateTime = maxLevitateTime;
			allowJump = true;
		}
		if(levitateTime <= 0)
		{
			levitateTime = 0;
			allowJump = false;
		}
		if(!isJumping && levitateTime < maxLevitateTime && grounded)
		{
			allowJump = true;
			levitateTime += Time.deltaTime;
		}
	}

	void DoMovement()
	{
		m_rigidbody.velocity = Vector2.zero;
		dist = m_moveDirection;

		if(Physics2D.CircleCast(new Vector2(m_transform.position.x,m_transform.position.y - (0.65f * 2)),(0.3f * 2), -Vector2.up,(0.1f * 2),layer))
			grounded = true;
		else
			grounded = false;
		
		if(Input.GetKey(KeyCode.Space) && allowJump)
		{
			if(lastParent != null && Vector3.Distance(m_transform.position,lastParent.transform.position) > (25f * 2))
			{
				lastParent = null;
			}

			isJumping = true;
			levitateTime -= Time.deltaTime;
			if(lastParent != null && lastParent.velocity.y >= 0)
				yVelocity = jumpSpeed + (lastParent.velocity.y * (25 * 2));
			else
				yVelocity = jumpSpeed;
			jumpParticle.enableEmission = true;
		}
		else
		{
			jumpParticle.enableEmission = false;
			isJumping = false;
			yVelocity += Physics2D.gravity.y * fallingSpeed * Time.deltaTime;
		}

		if (grounded && yVelocity < 0) 
		{
			yVelocity = Physics2D.gravity.y * Time.deltaTime;
		}

		dist.y = yVelocity * Time.deltaTime;

		m_moveDirection = new Vector2(Input.GetAxis("Horizontal"),Input.GetAxis("Vertical"));
		m_moveDirection = m_transform.TransformDirection(m_moveDirection);
		m_moveDirection *= speed;

//		if(movingPlatform != null)
//		{	
//			dist += movingPlatform.GetComponent<MovingPlatform>().velocity;
//		}

		m_rigidbody.velocity = dist;
	}

	private MovingPlatform lastParent;

	void OnCollisionEnter2D(Collision2D col) 
	{
		if(col.gameObject.layer == 17)
		{
			movingPlatform = col.transform;
			m_transform.SetParent(movingPlatform,true);
		}
	}

	void OnCollisionExit2D(Collision2D col) 
	{
		if(col.gameObject.layer == 17)
		{
			lastParent = m_transform.parent.GetComponent<MovingPlatform>();
			m_transform.parent = null;
			movingPlatform = null;
		}
	}
}
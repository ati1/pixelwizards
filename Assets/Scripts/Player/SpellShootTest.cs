﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpellShootTest : MonoBehaviour {

	public GameObject spellToFire;
	public Transform aim;
	private Stats stats;
	public List<Stats.SpellDmg> spellDmgs = new List<Stats.SpellDmg>();

	void Start()
	{
		if (aim == null)
			aim = GetComponent<Aim> ().transform;

		spellDmgs = transform.root.GetComponent<Stats> ().spellDmgs;

	}
	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.Mouse0))
		{
			GameObject fireball = GameObject.Instantiate (spellToFire, aim.position, aim.rotation) as GameObject;
			foreach(Stats.SpellDmg spellDmg in spellDmgs)
			{
				if(spellDmg.spellSchool == fireball.GetComponent<FireBall>().spellSchool)
				{
					fireball.GetComponent<FireBall>().dmg += spellDmg.dmg;
				}
			}
		}
	}		
}
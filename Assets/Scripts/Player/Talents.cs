﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Talents : MonoBehaviour {

	public bool showTalentsMenu = false;
	public int freeTalentPoints = 0;

	public int maxActiveTalents = 4;


	[System.Serializable]
	public class TalentTree
	{
		public string name = "";
		public int amount = 0;
		public bool toggled = false;
		public Spell.SpellSchool talentTreeType;

		public List<Spell> spellList;
	}
	
	public List<TalentTree> talentTrees;
	public List<Spell> choosedSpells;

	public Text freePoints;
	public List<GameObject> talents;
	
	public bool ismultiPlayer;

	void Awake()
	{
		ShowTalents("FireTalents");
	}
	
	public void InitMultiplayer()
	{
		
		for(int i = 0;i<talentTrees.Count;i++)
		{
			talents.Clear();
			talents.Add(GameObject.Find(talentTrees[i].name));	
		}
		
		ShowTalents("FireTalents");
	}
	
	public Spell GetSpellByName(string talentTree, string spellName)
	{
		Spell returnVal = null;
		for( int i = 0;i<talentTrees.Count;i++)
		{
			if(talentTrees[i].name == talentTree)
			{
				for( int j = 0;j<talentTrees[i].spellList.Count;j++)
				{
					if(talentTrees[i].spellList[j].spellName == spellName)
					{
						returnVal = talentTrees[i].spellList[j];
					}
				}
			}
		}
		
		return returnVal;
	}

	void Update()
	{
		freePoints.text = freeTalentPoints.ToString();
		if(Input.GetKeyDown(KeyCode.N) && !showTalentsMenu)
		{
			showTalentsMenu = true;
		}
		else if(Input.GetKeyDown(KeyCode.N) && showTalentsMenu)
		{
			showTalentsMenu = false;
		}
	}

	public void AddTalent(Spell spell)
	{
		if(choosedSpells.Count == 0)
		{
			choosedSpells.Add (spell);
			freeTalentPoints -= 1;
			SetTalents();
		}
		else
		{
			bool check = false;
			for(int i = 0; i<choosedSpells.Count;i++)
			{
				if(choosedSpells[i].spellName == spell.spellName)
				{
					check = true;
				}
			}
			if(!check)
			{
				choosedSpells.Add (spell);
				freeTalentPoints -= 1;
				SetTalents();
			}
		}
	}
	public void RemoveTalent(Spell spell)
	{
		List<ActiveSpells.ActionButton> abs = GetComponent<ActiveSpells> ().actionButtons;
		for(int i = 0;i<abs.Count;i++)
		{
			if(choosedSpells.Count == 0)
			{
				ResetActionButton(abs[i]);
			}
			else
			{
				for(int j = 0; j<choosedSpells.Count;j++)
				{
					if(choosedSpells[j].spellName == spell.spellName && abs[i].buttonID == j)
					{
						choosedSpells.Remove (choosedSpells[j]);
						ResetActionButton(abs[i]);
						freeTalentPoints += 1;
						//SetTalents();
					}
				}
			}
		}
	}
	void SetTalents()
	{
		List<ActiveSpells.ActionButton> abs = GetComponent<ActiveSpells> ().actionButtons;
		for(int i = 0;i<abs.Count;i++)
		{
			for(int j = 0;j<choosedSpells.Count;j++)
			{
				if(j == abs[i].buttonID)
				{
					abs[i].spell = choosedSpells[j];
					abs[i].text = choosedSpells[j].spellName;
					abs[i].img.gameObject.SetActive (true);
					abs[i].img.sprite = choosedSpells[j].img;
				}
			}
		}
	}
	
	void ResetActionButton(ActiveSpells.ActionButton ab)
	{
		ab.text = "N/A";
		ab.img.gameObject.SetActive (false);
		ab.spell = null;
		ab.cooldownTimer = 0;
		ab.canCast = false;
	}

	//-----------------------NEW UI STUFF-----------------------------//
	public void ShowTalents(string pushedButton)
	{
		foreach(GameObject obj in talents)
		{
			if(obj.name == pushedButton)
				obj.SetActive(true);
			else
				obj.SetActive(false);
		}
	}
	public void SetTalent(Spell spell)
	{
		if(freeTalentPoints > 0)
		{
			AddTalent(spell);
		}
		else
		{
			Debug.Log("No talentpoints left");
		}
	}
//		void OnGUI()
//		{
//			if(showTalentsMenu)
//			{
//				GUILayout.BeginArea(new Rect(50,50,1000,1000));
//				GUILayout.Label("Points Left: " + (int)freeTalentPoints,GUILayout.Width(200),GUILayout.Height(25));
//				foreach(TalentTree tree in talentTrees)
//				{
//					if(GUILayout.Button(tree.name,GUILayout.Width(100),GUILayout.Height(25)))
//					{
//						if(tree.toggled)
//							tree.toggled = false;
//						else
//							tree.toggled = true;
//					}
//					if(tree.toggled)
//					{
//						GUILayout.BeginVertical();
//						foreach(Spell spell in tree.spellList)
//						{
//							GUILayout.BeginHorizontal();
//							for(int i = 0;i<choosedSpells.Count;i++)
//							{
//								if(choosedSpells[i].spellName == spell.spellName)
//								{
//									GUILayout.Label((i + 1).ToString(),GUILayout.Width(25),GUILayout.Height(25));
//									GUI.color = Color.green;
//								}
//							}
//							if(GUILayout.Button(spell.spellName,GUILayout.Width(100),GUILayout.Height(50)))
//							{
//								if(freeTalentPoints > 0)
//								{
//									AddTalent(spell);
//								}
//								else
//								{
//									Debug.Log("No talentpoints left");
//								}
//							}
//							GUI.color = Color.white;
//							GUI.color = Color.red;
//							if(GUILayout.Button("X",GUILayout.Width(25),GUILayout.Height(25)))
//							{
//								RemoveTalent(spell);
//							}
//							GUI.color = Color.white;
//							GUILayout.EndHorizontal();
//	
//						}
//						GUILayout.EndVertical();
//					}
//				}
//				GUILayout.EndArea();
//			}
//		}
}
﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
[RequireComponent(typeof(NetworkView))]
public class PlayerNetworkValues : MonoBehaviour {

	private Camera playerCamera;

	public int playerID;
	public string playerName;
	public Stats stats;
	//Player spells
	public ActiveSpells activeSpells;
	public List<ActiveSpells.ActionButton> actionButtonList;
	public List<Spell> spells;
	//Player anim variables
	private Animator anim;
	private Transform playerSkeleton;
	//Player movement variables
	private Transform m_transform;
	private Rigidbody2D m_rigidbody2d;
	private float m_syncTime; 
	private float m_syncDelay;
	private float m_lastSynchronizationTime;
	private Vector2 m_syncStartPosition;
	private Vector2 m_syncEndPosition;


	private bool setUp;

	void Awake()
	{
		if(Network.isClient || Network.isServer)
		{
			m_transform = transform;
			playerSkeleton = m_transform.Find ("PlayerSkeleton");
			m_rigidbody2d = GetComponent<Rigidbody2D>();
			anim = GetComponentInChildren<Animator>();
			stats = GetComponent<Stats>();
			activeSpells = GetComponent<ActiveSpells>();
			actionButtonList = activeSpells.actionButtons;
			//Get all possible spells
			foreach(Talents.TalentTree tree in GetComponent<Talents>().talentTrees)
			{
				foreach(Spell spell in tree.spellList)
				{
					spells.Add(spell);
				}
			}

			m_lastSynchronizationTime = Time.time;

			if(GetComponentInChildren<Camera>())
				playerCamera = gameObject.GetComponentInChildren<Camera> ();
			
			if(GetComponent<NetworkView>().isMine)
			{
				playerCamera.gameObject.SetActive(true);
				gameObject.GetComponent<Movement>().enabled = true;
//				gameObject.GetComponent<Inventory>().enabled = true;
				gameObject.GetComponent<Talents>().enabled = true;
				gameObject.GetComponentInChildren<Aim>().enabled = true;
				gameObject.GetComponentInChildren<AnimationScript>().enabled = true;
				gameObject.GetComponent<Rigidbody2D>().isKinematic = false;
			}
			else if(!GetComponent<NetworkView>().isMine)
			{
				playerCamera.gameObject.SetActive(false);
				gameObject.GetComponent<Movement>().enabled = false;
//				gameObject.GetComponent<Inventory>().enabled = false;
				gameObject.GetComponent<Talents>().enabled = false;
				gameObject.GetComponentInChildren<Aim>().enabled = false;
				gameObject.GetComponentInChildren<AnimationScript>().enabled = false;
				gameObject.GetComponent<Rigidbody2D>().isKinematic = true;
			}
		}
	}
	float GetRoundedAmount(float number)
	{
		number *= 100;
		Mathf.Round(number);
		number /= 100;
		return number;
	}
	void FixedUpdate()
	{
		if(Network.isClient || Network.isServer)
		{
			if(!GetComponent<NetworkView>().isMine)
				SyncedMovement();
		}
	}
	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) 
	{
		float xPos = GetRoundedAmount(m_transform.position.x);
		float yPos = GetRoundedAmount(m_transform.position.y);
		float xVel = GetRoundedAmount(m_rigidbody2d.velocity.x);
		float yVel = GetRoundedAmount(m_rigidbody2d.velocity.y);

		float xScale = playerSkeleton.localScale.x;

		float speed = anim.GetFloat ("speed");
		bool grounded = anim.GetBool ("grounded");
		bool isLevitating = anim.GetBool ("isLevitating");

		if (stream.isWriting) 
		{
			stream.Serialize(ref xVel);
			stream.Serialize(ref yVel);

			stream.Serialize(ref xPos);
			stream.Serialize(ref yPos);
			stream.Serialize(ref xScale);

			stream.Serialize(ref speed);
			stream.Serialize(ref grounded);
			stream.Serialize(ref isLevitating);
		} 
		else 
		{
			stream.Serialize(ref xVel);
			stream.Serialize(ref yVel);

			stream.Serialize(ref xPos);
			stream.Serialize(ref yPos);
			stream.Serialize(ref xScale);

			stream.Serialize(ref speed);
			anim.SetFloat("speed",speed);
			stream.Serialize(ref grounded);
			anim.SetBool("grounded", grounded);
			stream.Serialize(ref isLevitating);
			anim.SetBool("isLevitating", isLevitating);

			playerSkeleton.localScale = new Vector3(xScale,playerSkeleton.localScale.y,playerSkeleton.localScale.z);
			m_transform.position = new Vector2(xPos,yPos);

			m_syncTime = 0f;
			m_syncDelay = Time.time - m_lastSynchronizationTime;
			m_lastSynchronizationTime = Time.time;		
			m_syncEndPosition = new Vector2(m_transform.position.x,m_transform.position.y) + new Vector2(xVel,yVel) * m_syncDelay;
			m_syncStartPosition = m_rigidbody2d.position;
		}
	}

	private void SyncedMovement()
	{
		m_syncTime += Time.deltaTime;
		m_rigidbody2d.position = Vector2.Lerp(m_syncStartPosition, m_syncEndPosition, m_syncTime / m_syncDelay);
	}
	void OnDisconnectedFromServer(NetworkDisconnection info)
	{
		Network.RemoveRPCs(GetComponent<NetworkView>().viewID);
		Network.Destroy (GetComponent<NetworkView>().viewID);
	}
	[RPC]
	void SetPlayerName(string m_name)
	{
		playerName = m_name;
	}
	[RPC]
	void SetPlayerId(int m_id)
	{
		playerID = m_id;
	}
	[RPC]
	public void UseSpell(string spellName, Vector3 pos, Quaternion quaternion)
	{
		foreach(Spell spell in spells)
		{
			if(spellName == spell.spellName)
			{
				GameObject temp = GameObject.Instantiate(spell.gameObject,pos,quaternion) as GameObject;
				Spell tempSpell = temp.GetComponent<Spell>();
				tempSpell.shooter = stats;
				tempSpell.CastSpell();
			}
		}
	}
	[RPC]
	public void TakeSpellDmgRPC(int dmg, int spellSchool)
	{
		stats.TakeSpellDmg (dmg, (Spell.SpellSchool)spellSchool);
	}
}
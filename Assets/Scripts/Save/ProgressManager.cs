﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ProgressManager : MonoBehaviour {

	public int levelProgress;

	public void DoEvents()
	{
		DestroyObject[] dOjects = FindObjectsOfType<DestroyObject> ();
		MoveObject[] mObjects = FindObjectsOfType<MoveObject> ();
		ShrinkObject[] sObjects = FindObjectsOfType<ShrinkObject> ();

		if(dOjects.Length > 0)
		{
			foreach(DestroyObject dObject in dOjects)
			{
				if(levelProgress >= dObject.progressValue)
					dObject.DoTrigger();
			}
		}

		List<MoveObject> moveObjects = new List<MoveObject> ();
		if(mObjects.Length > 0)
		{
			for(int i = 0;i<mObjects.Length;i++)
			{
				if(mObjects[i].triggerValue > 0)
				{
					moveObjects.Add(mObjects[i]);
				}
			}
		}
		foreach(MoveObject mObj in moveObjects)
		{
			if(levelProgress >= mObj.triggerValue)
				mObj.DoTrigger();
		}

		if(sObjects.Length > 0)
		{
			foreach(ShrinkObject sObject in sObjects)
			{
				if(levelProgress >= sObject.progressValue)
					sObject.Scale();
			}
		}
	}

	public void AddProgress(int amount)
	{
		levelProgress += amount;
	}
}
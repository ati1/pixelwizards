﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public static class SaveDataList 
{
	public static List<SaveGame.SaveData> savedGames = new List<SaveGame.SaveData>();
    public static List<SaveGame.SaveData> savedGamesServer = new List<SaveGame.SaveData>();
}
﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

public class SaveGame : MonoBehaviour {

//	private string path = Application.streamingAssetsPath + "/SaveData/";
//	private const string fileName = "SaveGame.xml";

	public SaveData currentData;
	public SaveMenu saveMenu;

	private bool m_hasPlayer;
	public bool dontAddProgress;
	
	public void Init()
	{
		SaveDataList.savedGames.Clear ();
		string[] files = Directory.GetFiles(Application.streamingAssetsPath + @"\");
			
		if(files.Length > 0)
		{
			foreach(string file in files)
			{
				if(file.Contains(".pw") && !file.Contains(".meta"))
				{
					//Debug.Log(file);
					SaveData data = new SaveData();
					data.path = file;
					Debug.Log(data.path);
					string[] split = file.Split(@"\"[0]);
					data.name = Path.GetFileNameWithoutExtension(split[split.Length - 1]);
					//Debug.Log(data.name);
					data.DeSerializeSaveData();
					SaveDataList.savedGames.Add(data);
					saveMenu.currentSavedGames++;
				}
			}
		}

		for(int j = 0;j<SaveDataList.savedGames.Count;j++)
		{ 
			Debug.Log(SaveDataList.savedGames[j].path + " " + j);
		}
			
//			if(File.Exists(Application.streamingAssetsPath + "/savedGames" + i + ".pw"))
//			{
//				SaveData data = new SaveData();
//				data.path = Application.streamingAssetsPath + "/savedGames" + i + ".pw";
//				data.DeSerializeSaveData();
//				SaveDataList.savedGames.Add(data);
//				saveMenu.currentSavedGames++;
//			}
	}
	
	public void LoadLevel(string lvl)
	{
		Application.LoadLevel(lvl);
	}

	void Awake()
	{
		if(GetComponent<Movement>())
		{
			m_hasPlayer = true;
		}
	}

	void OnGUI()
	{
		if(m_hasPlayer)
		{
			if(GUILayout.Button("Menu"))
			{
				MenuManager.LoadLevel("menu");
			}
		}
	}

	public void LoadGame()
	{
		if(currentData != null)
		{
			Stats stats = gameObject.GetComponent<Stats>();
			stats.ResetStats();
			stats.AddExp(currentData.totalExp);
			
			NewInventory inv = GetComponent<NewInventory> ();
			
			//ActiveSpells ab = GetComponent<ActiveSpells>();
			Talents talents = GetComponent<Talents>();
			
			for( int i = 0;i<currentData.equipSlots.Length;i++ )
			{
				if(currentData.equipSlots[i] != "")
				{
					inv.EquipItem(ItemDatabase.GetItemByName(currentData.equipSlots[i]));
					inv.equippedItems[i].hasItem = true;
					inv.GetComponent<Stats>().GetItemStats(inv.equippedItems[i].item);
				}
				else
				{
					if(inv.equippedItems[i].item != null)
					{
						inv.RemoveSprite(inv.equippedItems[i].item);
						inv.GetComponent<Stats>().RemoveItemStats(inv.equippedItems[i].item);
					}
						
					inv.equippedItems[i].EmptySlot();
					inv.equippedItems[i].hasItem = false;
				}
			}

            //TODO: This does not return all the items to their original positions and loses stack amounts,
            //it has been broken since beginning so you should probs look into it more
			for( int i = 0;i<currentData.inventorySlots.Length;i++ )
			{
				if(currentData.inventorySlots[i] != "")
				{
					inv.AddItem(ItemDatabase.GetItemByName(currentData.inventorySlots[i]));
					inv.inventorySlots[i].hasItem = true;
				}
				else
				{
					if(inv.inventorySlots[i].item != null)
						inv.RemoveItemFromInventory(i);
					inv.inventorySlots[i].hasItem = false;
				}
			}
			
			for( int i = 0;i<currentData.stackAmounts.Length;i++ )
			{
				if(currentData.stackAmounts[i] != 0)
				{
					inv.inventorySlots[i].currentStack = currentData.stackAmounts[i];
					inv.inventorySlots[i].UpdateStackText();
				}
			}
			
			for( int i = 0;i<currentData.activeSpells.Length;i++ )
			{
				if(currentData.activeSpells[i] != "")
				{
                    string talentTree = currentData.activeSpells[i].Split(',')[0];
                    string spellName = currentData.activeSpells[i].Split(',')[1];
                    talents.AddTalent(talents.GetSpellByName(talentTree, spellName));
				}
			}
			
			transform.position = currentData.pos;
			GetComponent<NewInventory> ().UpdateStatText();
			GetComponent<HealthBar>().AdjustHealthBar();

			ProgressManager prManager = FindObjectOfType<ProgressManager>();
			if(prManager != null && !dontAddProgress)
			{
				prManager.AddProgress(currentData.gameProgress);
				prManager.DoEvents();
			}

		}
	}

	[System.Serializable]
	public class SaveData : ISerializable 
	{
		public string name;

        //used by server saving
        public int dataID;
		
		public string path;
		public float x, y, z;
		public Vector3 pos;
		
		public NewInventory inventory;
		public string[] inventorySlots = new string[18];
		public int[] stackAmounts = new int[18];
		public string[] equipSlots = new string[6];
		
		public string[] activeSpells = new string[4];
		
		public Stats stats;
		public int totalExp;

		public int gameProgress;

		public string levelName = "";
		
		#region ISerializable implementation
		
		public void GetObjectData (SerializationInfo info, StreamingContext context)
		{
			//info.AddValue ("name", name);

			//add position
			info.AddValue ("x", x);
			info.AddValue ("y", y);
			info.AddValue ("z", z);
			
			//add level
			info.AddValue("exp", totalExp);
			info.AddValue("equipSlots", equipSlots);
			info.AddValue("stackAmounts", stackAmounts);
			info.AddValue("inventorySlots", inventorySlots);
			
			info.AddValue("activeSpells",activeSpells);

			info.AddValue ("gameProgress", gameProgress);

			info.AddValue ("levelName", levelName);
		}
		
		#endregion
		
		public SaveData(SerializationInfo info, StreamingContext context)
		{
			//name = (string)info.GetValue("name", name.GetType());

			//deserialize vector3 values
			x = (float)info.GetValue ("x", x.GetType());
			y = (float)info.GetValue ("y", y.GetType());
			z = (float)info.GetValue ("z", z.GetType());
			pos = new Vector3(x,y,z);
			
			//deserialize lvl info
			totalExp = (int)info.GetValue("exp",totalExp.GetType());
			equipSlots = (string[])info.GetValue("equipSlots", equipSlots.GetType());
			inventorySlots = (string[])info.GetValue("inventorySlots",inventorySlots.GetType());
			stackAmounts = (int[])info.GetValue("stackAmounts",stackAmounts.GetType());
			
			activeSpells = (string[])info.GetValue("activeSpells",activeSpells.GetType());

			gameProgress = (int)info.GetValue("gameProgress", gameProgress.GetType());

			levelName = (string)info.GetValue("levelName",levelName.GetType());
			Debug.Log(levelName);
		}
		
		public SaveData()
		{
		}
		
		public SaveData(Vector3 pos,NewInventory inventory,Stats stats)
		{
			x = pos.x;
			y = pos.y;
			z = pos.z;
			this.pos = new Vector3(x,y,z);
			
			this.inventory = inventory;
			this.stats = stats;
			
		}
		
		public void Serialize()
		{
			using (FileStream fileStream = new FileStream(path, FileMode.OpenOrCreate))
			{
				BinaryFormatter bf = new BinaryFormatter();
				bf.Serialize(fileStream, this);
			}
		}
		
		public SaveData DeSerializeSaveData()
		{
			FileStream fileStream = null;
			try
			{
				fileStream = new FileStream(path, FileMode.Open);
				Debug.Log(path);
				BinaryFormatter bf = new BinaryFormatter();
				return (SaveData)bf.Deserialize(fileStream);
			}
			catch
			{
				Debug.Log("Error while loading data");
				return new SaveData();
			}
			finally
			{
				if(fileStream != null)
					fileStream.Close();
			}
		}
	}
}
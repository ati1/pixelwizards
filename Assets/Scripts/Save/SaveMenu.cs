﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;

public class SaveMenu : MonoBehaviour {

	public SaveGame player;
	public int maxSavedGames, currentSavedGames;
	public Text logText;
	public InputField input;

	private bool m_showSavedGames;
	private bool m_levelWasLoaded;
	private bool m_inMenu;

    public string session_id;
    public SaveGame.SaveData saveDataServer;
    public string username;

	void Awake()
	{
		DontDestroyOnLoad (this);
		m_inMenu = true;
		Init ();
	}

	void Init()
	{
		if(m_inMenu)
		{
			player = FindObjectOfType<SaveGame>();
			player.saveMenu = this;

            if(GameObject.FindGameObjectWithTag("LogText"))
            {
                GameObject temp = GameObject.FindGameObjectWithTag("LogText");
                logText = temp.GetComponent<Text>();
                input = GameObject.FindGameObjectWithTag("SaveName").GetComponent<InputField>();
            }
		}
        else
		{
			player = FindObjectOfType<SaveGame>();
			player.saveMenu = this;

			logText = null;
		}
        
	}

    //loads the starter lobby and starts new game, saves it to the server
    public void StartNewGameServer(SaveGame.SaveData saveData)
    {
        Application.LoadLevel("lobby");
        StartCoroutine(StartingGameServer(saveData));
    }

    //start new game, this is used for savedata that is going to the server, called by StartNewGameServer(saveData) method
    public IEnumerator StartingGameServer(SaveGame.SaveData saveData)
    {
        while (!m_levelWasLoaded)
            yield return 1;
        m_levelWasLoaded = false;

        player = FindObjectOfType<SaveGame>();
        player.saveMenu = this;

        m_inMenu = false;
        NewInventory tempInv = FindObjectOfType<NewInventory>();
        Stats tempStats = tempInv.GetComponent<Stats>();

        tempInv.EquipItem(ItemDatabase.GetItemByName("Apprentice Robe"));
        tempStats.GetItemStats(ItemDatabase.GetItemByName("Apprentice Robe"));

        tempInv.EquipItem(ItemDatabase.GetItemByName("Apprentice Staff"));
        tempStats.GetItemStats(ItemDatabase.GetItemByName("Apprentice Staff"));

        tempInv.EquipItem(ItemDatabase.GetItemByName("Apprentice Hat"));
        tempStats.GetItemStats(ItemDatabase.GetItemByName("Apprentice Hat"));

        tempInv.UpdateStatText();

        saveData = new SaveGame.SaveData(player.transform.position, player.GetComponent<NewInventory>(), player.GetComponent<Stats>());
        player.currentData = saveData;

        //create new save for server
        FindObjectOfType<DBManager>().StartCoroutine(FindObjectOfType<DBManager>().CreateSaveData(saveDataServer));
    }

    //calls startnewgame coroutine, this is for local saves
    public void StartNewGame(string levelName, string saveDataName)
	{
		bool check = false;

		char[] name = saveDataName.ToCharArray ();
		for( int i = 0;i<name.Length;i++)
		{
			if(char.IsPunctuation(name[i]))
			{
				check = true;
			}
		}

		if(saveDataName != "" && !saveDataName.Contains("\n") && !saveDataName.Contains(" ") && !saveDataName.Contains("<")
		   && !saveDataName.Contains(">") && !check && saveDataName.Length <= 16)
		{
			Application.LoadLevel(levelName);
			StartCoroutine ("StartingGame", saveDataName);
		}
		else
		{
			logText.text = "Please enter a valid name for your save!";
		}
	}

    //init new game with starter gear
	public IEnumerator StartingGame(string saveDataName)
	{
		while (!m_levelWasLoaded)
			yield return 1;
		m_levelWasLoaded = false;

		player = FindObjectOfType<SaveGame> ();
		player.saveMenu = this;

		Debug.Log (saveDataName);
		
		m_inMenu = false;
		NewInventory tempInv = FindObjectOfType<NewInventory> ();
		Stats tempStats = tempInv.GetComponent<Stats> ();
		
		tempInv.EquipItem(ItemDatabase.GetItemByName("Apprentice Robe"));
		tempStats.GetItemStats (ItemDatabase.GetItemByName ("Apprentice Robe"));
		
		tempInv.EquipItem(ItemDatabase.GetItemByName("Apprentice Staff"));
		tempStats.GetItemStats (ItemDatabase.GetItemByName ("Apprentice Staff"));
		
		tempInv.EquipItem(ItemDatabase.GetItemByName("Apprentice Hat"));
		tempStats.GetItemStats (ItemDatabase.GetItemByName ("Apprentice Hat"));
		
		tempInv.UpdateStatText();
		
		SaveGame.SaveData saveData = new SaveGame.SaveData (player.transform.position,player.GetComponent<NewInventory>(),player.GetComponent<Stats>());
		saveData.path = Application.streamingAssetsPath + "/"+saveDataName+".pw";
		saveData.name = saveDataName;
		SaveDataList.savedGames.Add (saveData);
		player.currentData = saveData;
		currentSavedGames++;
		SaveGame ();
	}

    //IEnumerator that first loads level and then initializes it with savedata, behaves differently if we have session or not
	private IEnumerator LoadingGame(string saveDataName)
	{
        //check if we actually have level to load to
        bool cont = true;

        if (session_id != "" && saveDataName != "")
        {
            foreach (SaveGame.SaveData saveData in SaveDataList.savedGamesServer)
            {
                if (saveData.name == saveDataName)
                {
                    saveDataServer = saveData;
                    Application.LoadLevel(saveData.levelName);
                }
            }
        }
        else if(saveDataName != "")
        {
            foreach (SaveGame.SaveData saveData in SaveDataList.savedGames)
            {
                if (saveData.name == saveDataName)
                {
                    Application.LoadLevel(saveData.DeSerializeSaveData().levelName);
                }
            }
        }
        else
            cont = false;

        if(cont)
        {
            while (!m_levelWasLoaded)
            {
                yield return 1;
            }
            m_levelWasLoaded = false;

            m_inMenu = false;
            player = FindObjectOfType<SaveGame>();
            player.saveMenu = this;

            if (session_id != "")
                LoadSaveDataFromServer(saveDataName);
            else
                LoadSaveData(saveDataName);
        }
	}
	
    //called when using portal to travel from level to other
	public IEnumerator ChangeLevel(object[] args)
	{
		string lvlName = (string)args [0];
		string saveDataName = (string)args[1];
		Vector3 startPos = new Vector3((float)args [2],(float)args[3],(float)args[4]);

		Application.LoadLevel (lvlName);
		while (!m_levelWasLoaded)
		{
			yield return 1;
		}
		m_levelWasLoaded = false;
		
		m_inMenu = false;
		player = FindObjectOfType<SaveGame> ();
		player.saveMenu = this;
		player.dontAddProgress = true;

        if (session_id != "")
            LoadSaveDataFromServer(saveDataName);
        else
            LoadSaveData(saveDataName);

        player.currentData.gameProgress = 0;
		player.transform.position = startPos;
	}

	private void OnLevelWasLoaded(int index)
	{
		if(index == 0)
		{
			m_inMenu = true;
		}
		else
		{
			m_inMenu = false;
			m_levelWasLoaded = true;
		}
		Init ();
	}

    //called to save our game, depending if we have session or not, it will either save to local folder or to server
	public void SaveGame()
	{
		if(player.currentData != null)
		{
            player.currentData.x = player.transform.position.x;
			player.currentData.y = player.transform.position.y;
			player.currentData.z = player.transform.position.z;
			
			player.currentData.totalExp = player.GetComponent<Stats>().exp.totalExpEarned;
			
			NewInventory inventory = player.GetComponent<NewInventory>();
			List<string> equippedItemNames = new List<string>();
			foreach( Slot slot in inventory.equippedItems )
			{
				if(slot.item != null)
				{
					equippedItemNames.Add(slot.item.itemName);
				}
				else
				{
					equippedItemNames.Add("");
				}
			}
			
			List<string> inventorySlots = new List<string>();
			foreach( Slot slot in inventory.inventorySlots )
			{
				if(slot.item != null)
				{
					inventorySlots.Add(slot.item.itemName);
				}
				else
				{
					inventorySlots.Add("");
				}
			}

			List<int> stackAmounts = new List<int>();
			for(int i = 0;i<inventory.inventorySlots.Count;i++)
			{
				if(inventory.inventorySlots[i].item != null && inventory.inventorySlots[i].item.isStackable)
				{
					stackAmounts.Add(inventory.inventorySlots[i].currentStack);
				}
				else
				{
					stackAmounts.Add(0);
				}
			}
			
			Talents talents = player.GetComponent<Talents>();
			List<string> aSpellNames = new List<string>();
			ActiveSpells ab = player.GetComponent<ActiveSpells>();
			
			for(int i = 0;i<ab.actionButtons.Count;i++)
			{
				if(ab.actionButtons[i].spell != null)
				{
					for(int j = 0;j<talents.talentTrees.Count;j++)
					{
						if(talents.talentTrees[j].talentTreeType == ab.actionButtons[i].spell.spellSchool)
						{
							aSpellNames.Add(talents.talentTrees[j].name + "," + ab.actionButtons[i].spell.spellName);
						}
					}
				}
				else
				{
					aSpellNames.Add("");
				}
			}
			
			
			player.currentData.equipSlots = equippedItemNames.ToArray();
			player.currentData.inventorySlots = inventorySlots.ToArray();
			player.currentData.stackAmounts = stackAmounts.ToArray();
			player.currentData.activeSpells = aSpellNames.ToArray();

			ProgressManager prManager = FindObjectOfType<ProgressManager>();
			if(prManager != null)
				player.currentData.gameProgress = prManager.levelProgress;

			player.currentData.levelName = Application.loadedLevelName;

            //also save data to server if we have active session
            if(session_id != "")
            {
                //check if we just made the save data and dont have current id, then use the last one from server
                if(player.currentData.dataID == 0)
                {
                    player.currentData.dataID = FindObjectOfType<DBManager>().lastId;
                }
                saveDataServer = player.currentData;
                FindObjectOfType<DBManager>().Upload(saveDataServer);
            }
            //otherwise just use the old method and save it to computer
            else
            {
                player.currentData.Serialize();
            }

		}
	}

	public void LoadGame(string name)
	{
		StartCoroutine(LoadingGame(name));
	}
	
	public void DeleteSave(string name)
	{
		string path = Application.streamingAssetsPath + "/" + name + ".pw";
		
		if(System.IO.File.Exists(path))
		{
			System.IO.File.Delete(path);
		}
		
		MenuManager manager = FindObjectOfType<MenuManager>();
		currentSavedGames = 0;
		SaveDataList.savedGames.Clear ();
		player.Init();
		manager.DeleteButtons();
		manager.InitSaveData();
	}

    //server version of delete function, clears out our savedata and refreshes view, with new data from server
    public void DeleteSaveServer()
    {
        MenuManager manager = FindObjectOfType<MenuManager>();
        SaveDataList.savedGamesServer.Clear();
        manager.DeleteButtonsServer();

        //refresh save data from server
        FindObjectOfType<DBManager>().StartCoroutine(FindObjectOfType<DBManager>().GetSaveData());
        manager.InitSaveDataFromServer();
    }

    public void LoadSaveData(string name)
	{
		foreach(SaveGame.SaveData saveData in SaveDataList.savedGames)
		{
			if(saveData.name == name)
			{
				player.currentData = saveData.DeSerializeSaveData ();
				player.currentData.path = Application.streamingAssetsPath + "/" + name + ".pw";
				player.currentData.name = name;
				player.LoadGame ();
				player.GetComponent<NewInventory>().UpdateStatText();
				return;
			}
		}
	}

    //different version for server data
    public void LoadSaveDataFromServer(string savedataName)
    {
        foreach (SaveGame.SaveData saveData in SaveDataList.savedGamesServer)
        {
            if (saveData.name == savedataName)
            {
                player.currentData = saveData;
                player.currentData.name = saveData.name;
                player.LoadGame();
                player.GetComponent<NewInventory>().UpdateStatText();
                return;
            }
        }
    }
}
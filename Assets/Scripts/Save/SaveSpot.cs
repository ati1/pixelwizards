﻿using UnityEngine;
using System.Collections;

public class SaveSpot : MonoBehaviour {

	private bool m_allowSave;

	void OnTriggerEnter2D(Collider2D col)
	{
		SaveGame player = col.GetComponent<SaveGame> ();
		if(player != null)
		{
			m_allowSave = true;
		}
	}

	void OnTriggerExit2D(Collider2D col)
	{
		SaveGame player = col.GetComponent<SaveGame> ();
		if(player != null)
		{
			m_allowSave = false;
		}
	}

	void OnGUI()
	{
		if(m_allowSave)
		{
			if(GUI.Button(new Rect(Screen.width/2 - 100,Screen.height/2,100,30),"Save"))
			{
				FindObjectOfType<SaveMenu>().SaveGame();
			}
		}
	}
}
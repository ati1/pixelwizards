﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ServerHandler : MonoBehaviour {

	[System.Serializable]
	public class Player
	{
		public int playerID;
		public string playerName;
	}
	
	public Player player;
	public List<Player> playerList;
	//contains all methods that need to be called from server
	public static Dictionary<string, ServerDelegate> serverMethods;
	public static Dictionary<string, ServerDelegate> clientMethods;

	public delegate void ServerDelegate(object[] args);
	
	/// <summary>
	/// Inits the server method dictionary. Add new methods here.
	/// </summary>
	public void InitServerMethodDictionary()
	{
		serverMethods = new Dictionary<string, ServerDelegate>();
		serverMethods.Add("InitPlayer", delegate(object[] args) 
		{
			GetComponent<NetworkView>().RPC("UpdateClients",RPCMode.AllBuffered,"UpdatePlayerName",ToRPCString(args[0]));
		});
//		serverMethods.Add("AssingPlayerID", delegate(object[] args) 
//		{
//			List<object> temp = new List<object>();
//			int id = Network.connections.Length;
//			temp.Add(id);
//			networkView.RPC("UpdateClients",RPCMode.AllBuffered, ToRPCString(temp.ToArray()));
//		});
	}
	/// <summary>
	/// Inits the client method dictionary. Add methods that server does to clients here.
	/// </summary>
	public void InitClientMethodDictionary()
	{
		clientMethods = new Dictionary<string, ServerDelegate>();
		clientMethods.Add("UpdatePlayerName", delegate(object[] args) 
		{
			//FIND OUT WHICH PLAYER THIS IS !
			player.playerName = args[0].ToString();
		});
	}
	/// <summary>
	/// Inits the player.
	/// </summary>
	public void InitPlayer()
	{
		SendRequest ("InitPlayer", player.playerName);
	}
	/// <summary>
	/// Converts object array to string.
	/// </summary>
	/// <returns>The RPC string.</returns>
	/// <param name="args">Arguments.</param>
	public string ToRPCString(params object[] args)
	{
		string rpcString = "";
		foreach(object obj in args)
		{
			rpcString += obj.ToString() + "|" + obj.GetType().ToString() + ",";
		}
		Debug.Log (rpcString);
		return rpcString;
	}
	/// <summary>
	/// Gets the objects from RPC string.
	/// </summary>
	/// <returns>The objects from RPC string.</returns>
	/// <param name="rpcString">Rpc string.</param>
	public object[] GetObjectsFromRPCString(string rpcString)
	{
		List<object> objects = new List<object> ();
		
		char switchObj = ',';
		char getType = '|';
		
		string[] objs = rpcString.Split (switchObj);
		
		foreach(string obj in objs)
		{
			string[] objAndType = obj.Split(getType);
			string value = objAndType[0];
//			string type = objAndType[1];
//			Debug.Log(objAndType[0] + " : " + objAndType[1]);
			if(objAndType.Length == 2)
			{
				switch (objAndType[1])
				{
				case "System.Int32":
					objects.Add(System.Convert.ToInt32(value));
					break;
				case "System.String":
					objects.Add(value);
					break;
				}
			}
		}
		return objects.ToArray ();
		
	}
	/// <summary>
	/// Sends request to server.
	/// </summary>
	/// <param name="methodName">Method name.</param>
	/// <param name="args">Arguments.</param>
	public void SendRequest(string methodName, params object[] args)
	{
		if( methodName != "" )
		{
			GetComponent<NetworkView>().RPC ("HandleRequest", RPCMode.Server, methodName, ToRPCString(args));
		}
	}
	/// <summary>
	/// Handles clients requests based on name of method USED BY SERVER
	/// </summary>
	/// <param name="method">Method.</param>
	/// <param name="args">Arguments.</param>
	[RPC]
	public void HandleRequest(string method, string rpcString)
	{
		ServerDelegate methodD;
		serverMethods.TryGetValue( method, out methodD );
		
		if( methodD != null )
		{
			methodD(GetObjectsFromRPCString(rpcString));
		}
	}
	/// <summary>
	/// Updates the clients.
	/// </summary>
	/// <param name="method">Method.</param>
	/// <param name="args">Arguments.</param>
	[RPC]
	public void UpdateClients(string method, string rpcObjects)
	{
		object[] args = GetObjectsFromRPCString (rpcObjects);
		ServerDelegate methodD;
		clientMethods.TryGetValue( method, out methodD );
		
		if( methodD != null )
		{
			methodD(args);
		}
	}
}
﻿using UnityEngine;
using System.Collections;

public class SpawnHandler : MonoBehaviour {

	public GameObject[] enemies;
	public Transform[] spawnLocations;
	private float time;
	public float maxTime;

	// Use this for initialization
	void Start () {
		//Instantiate(enemies[0]);
	
	}
	
	// Update is called once per frame
	void Update () {

		time += 1 * Time.deltaTime;

		if (time > maxTime) 
		{
			for(int i = 0;i<enemies.Length;i++)
			{
				SpawnEnemy(i);
			}
			time = 0f;
		}
	
	}

	void SpawnEnemy(int ID)
	{
		Instantiate(enemies[ID],spawnLocations[Random.Range(0,spawnLocations.Length)].position,Quaternion.identity);
	}
}

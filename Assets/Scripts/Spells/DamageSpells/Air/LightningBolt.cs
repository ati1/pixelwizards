﻿using UnityEngine;
using System.Collections;

public class LightningBolt : Spell {
	
	public float launchSpeed;
	
	void Awake()
	{
		CastSpell = ShootLightningBolt;
	}
	void ShootLightningBolt()
	{
		launchSpeed += (shooter.GetComponent<Movement> ().GetCurrentSpeed() * 50);
		if(GetComponent<Rigidbody2D>())
		{
			GetComponent<Rigidbody2D>().AddForce(-transform.right * launchSpeed);
		}
		else
			Debug.LogError("Add rigidbody to bolt spell your fireing");
	}
}
﻿using UnityEngine;
using System.Collections;

public class ArcaneBlast : Spell {

	public float launchSpeed;

	void Awake()
	{
		CastSpell = ShootArcaneBlast;
	}

	void ShootArcaneBlast()
	{
		if(GetComponent<Rigidbody2D>())
		{
			//GetComponent<Rigidbody2D>().AddForce (-transform.right * launchSpeed);
			StartCoroutine("Accelerate");
		}
		else
			Debug.LogError("Add rigidbody to bolt spell your fireing");
	}

	IEnumerator Accelerate()
	{
		for (int i = 0; i < 100; i+=4) {
			yield return new WaitForSeconds (0.1f);
			GetComponent<Rigidbody2D>().AddForce (-transform.right * i);
		}
	}

	IEnumerator ArcaneWave()
	{
		for (int i = 0; i < 10; i++) 
		{
			yield return new WaitForSeconds (0.1f);

			transform.position = new Vector2(transform.position.x + 5, transform.position.y);
			GetComponent<Rigidbody2D>().AddForce(-transform.up * i);
		}
	}
}
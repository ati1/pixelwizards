﻿using UnityEngine;
using System.Collections;

public class ArcaneExplosion : Spell {

	private Animation anim;
	private bool animStarted = false;
	private Transform m_transform;

	void Awake () 
	{
		m_transform = transform;
		CastSpell = DoExplosion;
		anim = GetComponent<Animation> ();
		m_transform.rotation = Quaternion.identity;
	}

	void Update () 
	{
		if(animStarted)
		{
			m_transform.position = new Vector3(shooter.transform.position.x,
			                                   shooter.transform.position.y,
			                                   shooter.transform.position.z + 1);
			if(!anim.isPlaying)
			{
				Destroy(gameObject);
			}
		}
	}
	void DoExplosion()
	{
		animStarted = true;
		anim.Play ();
	}
}
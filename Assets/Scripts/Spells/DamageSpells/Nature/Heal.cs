﻿using UnityEngine;
using System.Collections;

public class Heal : Spell {
	
	private Transform m_transform;
	
	void Awake()
	{
		m_transform = GetComponent<Transform>();
		CastSpell = HealPlayer;
	}
	
	void HealPlayer()
	{
		shooter.Heal(dmg);
	}
	
	void FixedUpdate()
	{
		m_transform.position = new Vector3(shooter.transform.position.x,shooter.transform.position.y + .5f,shooter.transform.position.z);
	}
}
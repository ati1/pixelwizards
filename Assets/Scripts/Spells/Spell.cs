﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Spell : MonoBehaviour{

	public delegate void SpellDelegate();
	public SpellDelegate CastSpell;
	public Stats shooter;
	public string spellName = "";
	public float cooldown = 0;
	public Button button;
	public Sprite img;

	public enum SpellType
	{
		damage,
		heal
	};

	public SpellType spellType;
	public int manaCost;
		
	public enum SpellSchool
	{
		Fire,
		Arcane,
		Water,
		Nature,
		Air
	};
	public int dmg = 0;
	public SpellSchool spellSchool;
	public bool destroyOnHit = false;

	public GameObject dmgTexture;
	
	public bool isEnemySpell;

	public void SetDmg()
	{
		if(shooter != null)
		{
			dmg += shooter.dmg;
			foreach(Stats.SpellDmg splDmg in shooter.spellDmgs)
			{
				if(splDmg.spellSchool == spellSchool)
				{
					dmg += splDmg.dmg;
				}
			}
		}
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.GetComponent<Movement> () || col.gameObject.layer == 9) 
		{
			if(Network.isServer == true|| Network.isClient == true)
			{
				if(shooter.GetComponent<PlayerNetworkValues>().playerName != col.GetComponent<PlayerNetworkValues>().playerName)
				{
					col.GetComponent<PlayerNetworkValues>().GetComponent<NetworkView>().RPC("TakeSpellDmgRPC",RPCMode.AllBuffered, dmg, (int)spellSchool);
				}
			}
			else
			{
				if(isEnemySpell && col.GetComponent<Stats>())
				{
					int dmgAftherResi = col.GetComponent<Stats>().TakeSpellDmg(dmg,spellSchool);
					GameObject temp = GameObject.Instantiate(dmgTexture,col.transform.position,Quaternion.identity) as GameObject;
					CombatText text = temp.GetComponent<CombatText>();
					text.textMesh.text = "-"+dmgAftherResi.ToString();
				}
			}
		}
		else
		{
			if(col.GetComponent<Stats>() && !isEnemySpell)
			{
				switch (spellType)
				{
				case SpellType.damage:

					int dmgAftherResi = col.GetComponent<Stats>().TakeSpellDmg(dmg,spellSchool);
					GameObject temp = GameObject.Instantiate(dmgTexture,col.transform.position,Quaternion.identity) as GameObject;
					CombatText text = temp.GetComponent<CombatText>();
					text.textMesh.text = "-"+dmgAftherResi.ToString();
					break;
				case SpellType.heal:
					//add healing
					break;
				}
				
			}
			if(destroyOnHit)
				Destroy(gameObject);
		}
	} 
}
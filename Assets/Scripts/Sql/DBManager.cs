﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.Linq;
using System;

public class DBManager : MonoBehaviour
{
    private string dbUrl = "";

    private const string registerUrl = "/pixelwizards/register.php?";
    private const string loginUrl = "/pixelwizards/login.php?";
    private const string getSaveDataUrl = "/pixelwizards/getSavedata.php?";
    private const string uploadSaveDataUrl = "/pixelwizards/uploadSavedata.php?";
    private const string logoutUrl = "/pixelwizards/logout.php?";
    private const string createSaveDataUrl = "/pixelwizards/createSavedata.php?";
    private const string getStatisticsUrl = "/pixelwizards/getStatistics.php?";
    private const string deleteSaveDataUrl = "/pixelwizards/deleteSavedata.php?";

    public string testReturn;

    public Text username;
    public InputField password;

    public string loggedInUser;
    public string session_id;
    public int lastId;

    private void Start()
    {
        //get the session_id from SaveMenu as soon as possible
        session_id = FindObjectOfType<SaveMenu>().session_id;
        //already session in progress
        if (session_id != "")
        {
            RefreshSession();
        }     
    }

    #region UIFunctions

    //if there is active session already when this script loads, show session view, if in menu and cache logged in user
    public void RefreshSession()
    {
        loggedInUser = FindObjectOfType<SaveMenu>().username;
        if (FindObjectOfType<MenuManager>())
        {
            FindObjectOfType<MenuManager>().ShowLoggedInUI(loggedInUser);
            FindObjectOfType<MenuManager>().ShowLogInTab();

            StartCoroutine(GetSaveData());
            StartCoroutine(GetStatistics());
        }
    }

    public void Register()
    {
        StartCoroutine(RegisterUser());
    }

    public void LogIn()
    {
        StartCoroutine(LogInUser());
    }

    public void LogOut()
    {
        StartCoroutine(LogOutUser());
    }

    public void Upload(SaveGame.SaveData saveData)
    {
        StartCoroutine(UploadSaveData(saveData));
    }

    public void StartNewGameServer()
    {
        FindObjectOfType<SaveMenu>().StartNewGameServer(FindObjectOfType<SaveGame>().currentData);
    }

    #endregion

    /// <summary>
    /// IEnumerator that calls register.php from our server and tries to register new user
    /// </summary>
    public IEnumerator RegisterUser()
    {
        //add required post values to our "form"
        WWWForm form = new WWWForm();
        form.AddField("username", username.text);
        form.AddField("password", password.text);

        //send the request to our php server
        WWW register = new WWW(dbUrl + registerUrl, form);
        yield return register;

        if (register.error != null)
            print("There was an error trying to connect to " + register.url + " " + register.error);
        else
        {
            //handle return value
            print("Connection success to " + register.url);
            testReturn = register.text;
            print(testReturn);
        }

    }

    /// <summary>
    /// IEnumerator that calls login.php from our server and tries to login user and save that users "session_id" variable.
    /// It will also check if user has any save data and load that, if there is data.
    /// </summary>
    public IEnumerator LogInUser()
    {
        WWWForm form = new WWWForm();
        form.AddField("username", username.text);
        form.AddField("password", password.text);

        WWW login = new WWW(dbUrl + loginUrl, form);
        yield return login;

        if(login.error != null)
            print("There was an error trying to connect to " + login.url + " " + login.error);
        else
        {
            print("Connection success to " + login.url);
            testReturn = login.text;
            print(testReturn);

            //this is very bad way, figure out better
            if (login.text != "" && login.text != "Invalid credentials")
            {
                loggedInUser = username.text;
                session_id = login.text;

                //save session id here to nondestroy object so we can carry it around
                FindObjectOfType<SaveMenu>().session_id = session_id;
                FindObjectOfType<SaveMenu>().username = loggedInUser;

                 //if logged in succesfully show logged in text and hide login
                FindObjectOfType<MenuManager>().ShowLoggedInUI(FindObjectOfType<SaveMenu>().username);
                //get save data
                StartCoroutine(GetSaveData());
            }
        }        
    }

    /// <summary>
    /// Gets savedata from user with active session
    /// </summary>
    /// <returns></returns>
    public IEnumerator GetSaveData()
    {
        //get data from savedata table, if there is any
        WWWForm sesForm = new WWWForm();
        sesForm.AddField("session_id", session_id);
        WWW getData = new WWW(dbUrl + getSaveDataUrl, sesForm);
        yield return getData;

        if (getData.error != null)
            print("error accessing " + getData.url + " " + getData.error);
        else
        {
            print(getData.text);

            if (getData.text != "No data found" && getData.text != "")
            {

                LoadSaveFile(getData.text);
            }
            else
            {
                print("No save data found, create new game first!");
            }
        }
    }

    /// <summary>
    /// gets statistics from database
    /// </summary>
    /// <returns></returns>
    public IEnumerator GetStatistics()
    {
        //no query variables needed
        WWW getData = new WWW(dbUrl + getStatisticsUrl);
        yield return getData;

        if (getData.error != null)
            print("error accessing " + getData.url + " " + getData.error);
        else
        {
            string json = getData.text;
            json = "{ \"array\" : " + json + " }";
            
            ScoreArray sa = JsonUtility.FromJson<ScoreArray>(json);

            //format higscore text
            string s = string.Format("{0,8}{1,10}\n","NAME", "EXP");
            s += "-------------------------\n";
            foreach (Score sc in sa.array)
                s += string.Format("{0,8} {1,13:N0}\n", sc.username, sc.exp);

            //refresh scoreboard ingame
            FindObjectOfType<MenuManager>().topWizards.text = "Top Wizards:\n\n" + s;
        }
    }

    /// <summary>
    /// IEnumerator that converst our saveData to our database format 
    /// and attepms to call uploadSaveData.php from our server to upload the data to our database
    /// </summary>
    /// <param name="saveData"></param>
    public IEnumerator UploadSaveData(SaveGame.SaveData saveData)
    {
        //add all our save variables to our php request
        WWWForm sesForm = new WWWForm();
        sesForm.AddField("saveDataID", saveData.dataID);
        sesForm.AddField("session_id", FindObjectOfType<SaveMenu>().session_id);
        saveData.pos = new Vector3(saveData.x, saveData.y, saveData.z);
        sesForm.AddField("position", VectorToString(saveData.pos));
        sesForm.AddField("exp", saveData.totalExp);
        sesForm.AddField("equipSlots", string.Join(",", saveData.equipSlots));
        sesForm.AddField("inventorySlots", string.Join(",", saveData.inventorySlots));

        //linq jargon to convert int array to string array in oneliner... cba to write it the long way
        string[] stackAmountsString = saveData.stackAmounts.Select(x => x.ToString()).ToArray();
        sesForm.AddField("stackAmounts", string.Join(",", stackAmountsString));

        sesForm.AddField("activeSpells", string.Join(",", saveData.activeSpells));
        sesForm.AddField("gameProgress", saveData.gameProgress);
        sesForm.AddField("levelName", saveData.levelName);

        //send request with our variable data
        WWW uploadData = new WWW(dbUrl + uploadSaveDataUrl, sesForm);
        yield return uploadData;

        if (uploadData.error != null)
            print("error accessing " + uploadData.url + " " + uploadData.error);
        else
        {
            print(uploadData.text);
        }
    }

    /// <summary>
    /// IEnumerator that creates new savedata for user
    /// 
    /// </summary>
    /// <param name="saveData"></param>
    public IEnumerator CreateSaveData(SaveGame.SaveData saveData)
    {
        //add all our save variables to our php request
        WWWForm sesForm = new WWWForm();
        //sesForm.AddField("saveDataID", saveData.dataID);
        sesForm.AddField("session_id", FindObjectOfType<SaveMenu>().session_id);
        saveData.pos = new Vector3(saveData.x, saveData.y, saveData.z);
        sesForm.AddField("position", VectorToString(saveData.pos));
        sesForm.AddField("exp", saveData.totalExp);
        sesForm.AddField("equipSlots", string.Join(",", saveData.equipSlots));
        sesForm.AddField("inventorySlots", string.Join(",", saveData.inventorySlots));

        //linq jargon to convert int array to string array in oneliner... cba to write it the long way
        string[] stackAmountsString = saveData.stackAmounts.Select(x => x.ToString()).ToArray();
        sesForm.AddField("stackAmounts", string.Join(",", stackAmountsString));

        sesForm.AddField("activeSpells", string.Join(",", saveData.activeSpells));
        sesForm.AddField("gameProgress", saveData.gameProgress);
        sesForm.AddField("levelName", saveData.levelName);

        //send request with our variable data
        WWW createData = new WWW(dbUrl + createSaveDataUrl, sesForm);
        yield return createData;

        if (createData.error != null)
            print("error accessing " + createData.url + " " + createData.error);
        else
        {
            print(createData.text);
            lastId = int.Parse(createData.text);
            FindObjectOfType<SaveMenu>().SaveGame();
        }
    }

    /// <summary>
    /// IEnumerator that deletes save data from user
    /// 
    /// </summary>
    /// <param name="saveData"></param>
    public IEnumerator DeleteSaveData(string saveDataName)
    {
        foreach(SaveGame.SaveData sd in SaveDataList.savedGamesServer)
        {
            if (sd.name == saveDataName)
            {
                WWWForm sesForm = new WWWForm();
                sesForm.AddField("saveDataID", sd.dataID);
                sesForm.AddField("session_id", FindObjectOfType<SaveMenu>().session_id);

                //send request with our variable data
                WWW deleteData = new WWW(dbUrl + deleteSaveDataUrl, sesForm);
                yield return deleteData;

                if (deleteData.error != null)
                    print("error accessing " + deleteData.url + " " + deleteData.error);
                else
                {
                    print(deleteData.text);
                    //remove the save button from the list here
                    //also delete save data locally from list and savemenu object
                }
                //delete buttons and save data locally
                FindObjectOfType<SaveMenu>().DeleteSaveServer();
                break;
            }
        }
    }


    /// <summary>
    /// IEnumerator that calls logout.php on our server and terminates our active session, on server and locally
    /// </summary>
    /// <returns></returns>
    public IEnumerator LogOutUser()
    {
        WWWForm form = new WWWForm();
        form.AddField("session_id", session_id);

        WWW logout = new WWW(dbUrl + logoutUrl, form);
        yield return logout;

        if (logout.error != null)
            print("There was an error trying to connect to " + logout.url + " " + logout.error);
        else
        {
            //if logout was succesfull, delete our session from our memory
            print("Connection success to " + logout.url);
            testReturn = logout.text;
            session_id = "";
            loggedInUser = "";
            FindObjectOfType<SaveMenu>().session_id = "";
            FindObjectOfType<SaveMenu>().saveDataServer = null;

            print(testReturn);
            FindObjectOfType<MenuManager>().ShowLogInUI();
            //clear our savelist as well..
            SaveDataList.savedGamesServer.Clear();
        }
    }

    /// <summary>
    /// Function that parses saveDataJson to our saveData format using unitys JsonUtility.FromJson() method.
    /// It "works" but there are few hacks in there
    /// </summary>
    /// <param name="saveDataJson"></param>
    public void LoadSaveFile(string saveDataJson)
    {
        //because jsonutility is even more ass we need to do this..
        //more here: https://forum.unity.com/threads/how-to-load-an-array-with-jsonutility.375735/
        saveDataJson = "{ \"array\" : " + saveDataJson + " }";

        print(saveDataJson);

        SavesdhArray sdha = JsonUtility.FromJson<SavesdhArray>(saveDataJson);

        //clear our static list before adding savedata into it
        SaveDataList.savedGamesServer.Clear();
        foreach (Savesdh sdh in sdha.array)
        {
            SaveGame.SaveData saveData = new SaveGame.SaveData();
            saveData.name = sdh.username + "_" + sdh.ID;
            saveData.dataID = sdh.ID;
            saveData.pos = StringToVector3(sdh.position);
            saveData.totalExp = sdh.exp;
            saveData.equipSlots = sdh.equipSlots.Split(',');
            saveData.inventorySlots = sdh.inventorySlots.Split(',');
            saveData.stackAmounts = Array.ConvertAll(sdh.stackAmounts.Split(','), int.Parse);

            //Our way of handling activeSpells data is done very wierdly, so here we parse it back into form that our savedata understands..
            List<string> activeS = new List<string>();
            string[] temp = sdh.activeSpells.Split(',');
            for (int i = 0; i < temp.Length; i++)
                if (i % 2 == 0 && temp[i] != "")
                    activeS.Add(string.Join(",", temp, i, 2));
            saveData.activeSpells = activeS.ToArray();

            saveData.gameProgress = sdh.gameProgress;
            saveData.levelName = sdh.levelName;

            //add them to safekeeping
            SaveDataList.savedGamesServer.Add(saveData);
        }
        //initialize savedata view
        FindObjectOfType<MenuManager>().InitSaveDataFromServer();
    }

    //unitys own jsonparser is 100% ass 
    //and cant even parse normal json into <string,string> dictionary 
    //so we have to create these fucking stupid holder classes

    [Serializable]
    public class SavesdhArray
    {
        public Savesdh[] array;
    }

    [Serializable]
    public class Savesdh
    {
        public string username;
        public int ID;
        public string position;
        public int exp;
        public string equipSlots;
        public string inventorySlots;
        public string stackAmounts;
        public string activeSpells;
        public int gameProgress;
        public string levelName;
    }

    [Serializable]
    public class ScoreArray
    {
        public Score[] array;
    }

    [Serializable]
    public class Score
    {
        public string username;
        public int exp;
    }

    //helper functions for conversion
    public static string VectorToString(Vector3 theV)
    {
        return "(" + theV.x + ", " + theV.y + ", " + theV.z + ")";
    }

    public static Vector3 StringToVector3(string sVector)
    {
        // Remove the parentheses
        if (sVector.StartsWith("(") && sVector.EndsWith(")"))
        {
            sVector = sVector.Substring(1, sVector.Length - 2);
        }

        // split the items
        string[] sArray = sVector.Split(',');

        // store as a Vector3
        Vector3 result = new Vector3(
            float.Parse(sArray[0]),
            float.Parse(sArray[1]),
            float.Parse(sArray[2]));

        return result;
    }
}

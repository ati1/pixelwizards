﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Stats : MonoBehaviour {
	
	public int health = 0;
	public int maxHealth = 0;
	public int healingTaken = 0;
	public int armor = 0;
	public int dmg = 0;
	public bool isDead = false;

	public ParticleSystem levelUpParticle;

	private bool m_isImmune;
	
	public void SetImmune(bool value)
	{
		m_isImmune = value;
	}
	
	[SerializeField]
	private GameObject m_dieUI;
	

	[System.Serializable]
	public class SpellResistance
	{
		public int amount;
		public Spell.SpellSchool spellSchool;
	}
	[System.Serializable]
	public class SpellDmg
	{
		public int dmg;
		public Spell.SpellSchool spellSchool;
	}
	[System.Serializable]
	public class Experience
	{
		public HealthBar hpBar;
		public int currentLvl;
		public int maxLvl;
		public int currentExp;
		public int expToLvl;
		
		public int totalExpEarned;
		public int expIncrease;

		public int GetCurrentLvl()
		{
			return currentLvl;
		}
		public int GetExpToNextLvl()
		{
			return expToLvl - currentExp;
		}
	}
	public List<SpellResistance> spellResistances = new List<SpellResistance>
	{
		new SpellResistance{spellSchool = Spell.SpellSchool.Arcane},
		new SpellResistance{spellSchool = Spell.SpellSchool.Air},
		new SpellResistance{spellSchool = Spell.SpellSchool.Fire},
		new SpellResistance{spellSchool = Spell.SpellSchool.Nature},
		new SpellResistance{spellSchool = Spell.SpellSchool.Water}
	};
	public List<SpellDmg> spellDmgs = new List<SpellDmg>
	{
		new SpellDmg{spellSchool = Spell.SpellSchool.Arcane},
		new SpellDmg{spellSchool = Spell.SpellSchool.Air},
		new SpellDmg{spellSchool = Spell.SpellSchool.Fire},
		new SpellDmg{spellSchool = Spell.SpellSchool.Nature},
		new SpellDmg{spellSchool = Spell.SpellSchool.Water}
	};
	public Experience exp;
	
	public bool dropsLoot;

	void Start()
	{
		if(transform.gameObject.tag == "Player")
		{
			levelUpParticle.Stop();
			if(m_dieUI != null)
			{
				m_dieUI.SetActive(false);
			}
		}

		if(GetComponent<NewInventory>())
			GetComponent<NewInventory>().UpdateStatText();
			
	}
	
	public void AddStatsByLvl(int lvl)
	{
		health += lvl;
		maxHealth += lvl;
		//armor += lvl;
		dmg += lvl;
	}
	int GetExpAmount()
	{
		return exp.currentLvl * (int)Mathf.Pow(2,4);
	}
	public void AddExp(int amount)
	{
		if(isDead)
			return;

		if(exp.currentLvl < exp.maxLvl)
		{
			exp.currentExp += amount;
			exp.totalExpEarned += amount;
			
			if(exp.currentExp > exp.expToLvl)
			{
				LvlUp();
			}
		}
		if(exp.currentLvl == exp.maxLvl)
			exp.currentExp = 0;
		if(GetComponent<NewInventory>())
			GetComponent<NewInventory>().UpdateStatText();

	}
	public void LvlUp()
	{
		levelUpParticle.Play();

		int overLvl = exp.currentExp - exp.expToLvl;
		exp.currentExp = overLvl;

		
		exp.expToLvl = exp.currentLvl + exp.expIncrease * (int)Mathf.Pow(2,(float)exp.currentLvl/7);
		
		exp.currentLvl += 1;
		AddStatsByLvl(exp.currentLvl);

		if(exp.hpBar != null)
			exp.hpBar.AdjustHealthBar();
		if(GetComponent<NewInventory>())
			GetComponent<NewInventory>().UpdateStatText();
		
		if(exp.currentExp > exp.expToLvl)
		{
			LvlUp();
		}
	}
	public virtual int TakeSpellDmg(float dmg, Spell.SpellSchool spellSchool)
	{
		if(isDead || m_isImmune)
			return 0;
		float dmgAftherResi = 0;
		foreach(SpellResistance resi in spellResistances)
		{
			if(resi.spellSchool == spellSchool)
			{
				dmgAftherResi = dmg - (dmg * ((float)resi.amount / 100));
			}
		}
		health -= (int)dmgAftherResi;

		if (health <= 0)
		{
			health = 0;
			Die ();
		}
		if(GetComponent<HealthBar>())
		{
			HealthBar hpBar = GetComponent<HealthBar> ();
			hpBar.AdjustHealthBar ();
		}
		if(GetComponent<NewInventory>())
			GetComponent<NewInventory>().UpdateStatText();

		return (int)dmgAftherResi;
	}
	public void TakeMeleeDmg(float dmg)
	{
		if(isDead || m_isImmune)
			return;
		float dmgAftherArmor = dmg - (dmg * ((float)armor / 100));
		if(dmgAftherArmor >= 0)
		{
			health -= (int)dmgAftherArmor;
			HealthBar hpBar = GetComponent<HealthBar> ();
			hpBar.AdjustHealthBar ();
		}

		if (health <= 0)
		{
			Die ();
		}

		if(GetComponent<NewInventory>())
			GetComponent<NewInventory>().UpdateStatText();
	}
	public void Heal(float healthIncrease)
	{
		if(health < maxHealth)
		{
			int m_healthIncrease = health + (int)(healthIncrease + (healthIncrease * ((float)healingTaken / 100)));
			if(m_healthIncrease >= maxHealth)
			{
				health = maxHealth;
			}
			else
			{
				health += (int)healthIncrease;
			}

			exp.hpBar.AdjustHealthBar();
			if(GetComponent<NewInventory>())
				GetComponent<NewInventory>().UpdateStatText();
		}

	}
	public virtual void Die()
	{
		isDead = true;
		if(!GetComponent<Movement>() && exp.currentLvl > 0)
		{
			Movement[] players = FindObjectsOfType<Movement>();
			for(int i = 0;i<players.Length;i++)
			{
				players[i].GetComponent<Stats>().AddExp(GetExpAmount());
				//Debug.Log(GetExpAmount());
			}
			isDead = true;
			if(dropsLoot)
				GetComponent<Loot>().DropItems();
			
			GetComponent<Rigidbody2D>().isKinematic = true;
			GetComponent<BoxCollider2D>().enabled = false;;
			GetComponent<CircleCollider2D>().enabled = false;
			Destroy(gameObject,1);
		}
		else if(GetComponent<Movement>())
		{
			//player died
			GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
			GetComponent<Rigidbody2D>().isKinematic = true;
			Debug.Log("I'm dead lol");
			
			foreach(MonoBehaviour mono in GetComponents<MonoBehaviour>())
			{
				if(mono.name != "SaveGame")
					mono.enabled = false;
			}
			m_dieUI.SetActive(true);
		}
		else
		{
			if(dropsLoot)
				GetComponent<Loot>().DropItems();
			Destroy(gameObject);
		}
	}
	public void GetItemStats(ItemDatabase.Item item)
	{
		if(item != null)
		{
			health += item.health;
			maxHealth += item.health;
			armor += item.armor;
			dmg += item.dmg;

			if(item.itemFunction != "")
			{
				switch(item.itemFunction)
				{
				case "IncreaseHealingTaken":
					healingTaken += item.parameters[0].getIntValue();
					break;
				}
			}

			foreach(SpellDmg splDmg in spellDmgs)
			{
				foreach(SpellDmg itmSplDmg in item.spellDmg)
				{
					if(splDmg.spellSchool == itmSplDmg.spellSchool)
					{
						splDmg.dmg += itmSplDmg.dmg;
					}
				}
			}
			foreach(SpellResistance splRes in spellResistances)
			{
				foreach(SpellResistance itmSplRes in item.spellRes)
				{
					if(splRes.spellSchool == itmSplRes.spellSchool)
					{
						splRes.amount += itmSplRes.amount;
					}
				}
			}
			if(exp.hpBar != null)
			{
				exp.hpBar.AdjustHealthBar();
			}
		}
	}
	public void RemoveItemStats(ItemDatabase.Item item)
	{
		if(item != null)
		{
			health -=item.health;
			maxHealth -= item.health;
			armor -= item.armor;
			dmg -= item.dmg;

			if(item.itemFunction != "")
			{
				switch(item.itemFunction)
				{
				case "IncreaseHealingTaken":
					healingTaken -= item.parameters[0].getIntValue();
					break;
				}
			}

			foreach(SpellDmg splDmg in spellDmgs)
			{
				foreach(SpellDmg itmSplDmg in item.spellDmg)
				{
					if(splDmg.spellSchool == itmSplDmg.spellSchool)
					{
						splDmg.dmg -= itmSplDmg.dmg;
					}
				}
			}
			foreach(SpellResistance splRes in spellResistances)
			{
				foreach(SpellResistance itmSplRes in item.spellRes)
				{
					if(splRes.spellSchool == itmSplRes.spellSchool)
					{
						splRes.amount -= itmSplRes.amount;
					}
				}
			}
			if(exp.hpBar != null)
			{
				exp.hpBar.AdjustHealthBar();
			}
		}
	}
	public void ResetStats()
	{
		health = 100;
		maxHealth = 100;
		armor = 0;
		dmg = 0;
		
		exp.currentExp = 0;
		exp.currentLvl = 1;
		exp.totalExpEarned = 0;
		exp.expToLvl = 82;
		exp.expIncrease = 300;
		
	}
	public string GetStatsString()
	{
		string returnString = "";
		string tempSplDmgs = "";
		string TemosplResis = "";
		foreach(SpellDmg splDmg in spellDmgs)
		{
			tempSplDmgs += splDmg.spellSchool.ToString() + "Dmg% " + splDmg.dmg + "\n";
		}
		foreach(SpellResistance splRes in spellResistances)
		{
			TemosplResis += splRes.spellSchool.ToString() + "Resi% " + splRes.amount + "\n";
		}
		returnString += "Stats \n" + "Lvl " + exp.GetCurrentLvl().ToString() + "\n" + "Health " + health + "/" + maxHealth + "\n" + "Armor% " + armor + "\n" +
			"SpellDmg " + dmg + "\n\n" + tempSplDmgs + "\n" + TemosplResis + "\n" + "ExpToNextLvl " + exp.GetExpToNextLvl() + 
				"\n" + "TotalExp " + exp.totalExpEarned.ToString();
		return returnString;
	}
}
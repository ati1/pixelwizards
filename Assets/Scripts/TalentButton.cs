﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class TalentButton : MonoBehaviour, IPointerClickHandler {

	public enum TalentType
	{
		passive,
		active
	};
	
	public TalentType talentType;
	public Spell activeSpell;
	
	private Talents m_playerTalents;
	
	void Awake()
	{
		m_playerTalents = FindObjectOfType<Talents>();
	}
	
	public void RemoveTalent()
	{
		m_playerTalents.RemoveTalent(activeSpell);
	}
	
	public void AddTalent()
	{
		m_playerTalents.AddTalent(activeSpell);
	}

	public void OnPointerClick(PointerEventData data)
	{
		switch(data.button)
		{
		case PointerEventData.InputButton.Left:
			AddTalent();
			break;
		case PointerEventData.InputButton.Right:
			RemoveTalent();
			break;
		}
	}
	
}
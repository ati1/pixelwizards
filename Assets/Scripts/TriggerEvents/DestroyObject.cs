﻿using UnityEngine;
using System.Collections;

public class DestroyObject : MonoBehaviour {

	[SerializeField]
	private GameObject objectToDestroy;

	[SerializeField]
	private GameObject triggerer;

	public int progressValue;
	public int progressIncrease;
	
	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.transform == triggerer.transform)
		{
			FindObjectOfType<ProgressManager>().AddProgress(progressIncrease);
			DoTrigger();
		}
	}

	public void DoTrigger()
	{
		Destroy(objectToDestroy);
	}
}

﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {


	[SerializeField]
	private GameObject triggerer;
	
	[SerializeField]
	private GameObject spawnPoint;

	[SerializeField]
	private GameObject enemyToSpawn;

	[SerializeField]
	private int amount;

	private int dirSwitch;

	private bool triggered;

	[SerializeField]
	private bool ati = true;

	void Start () {

		dirSwitch = 0;
	}

	void SpawnEnemies()
	{
		for(int i = 0; i < amount; i++)
		{
			triggered = true;
			GameObject a = Instantiate(enemyToSpawn, new Vector2(spawnPoint.transform.position.x, spawnPoint.transform.position.y), Quaternion.identity) as GameObject;

			if(ati)
				a.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(-1f,1f),1) * Random.Range(20, 50),ForceMode2D.Impulse);	
			//a.transform.rigidbody.AddForce

		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.transform == triggerer.transform)
		{
			//Debug.Log("imhere");
			if(!triggered)
			{
				SpawnEnemies();
			}
		}
	}
}
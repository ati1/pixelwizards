﻿using UnityEngine;
using System.Collections;

public class LoadLevelTrigger : MonoBehaviour {

	public string lvlName;
	private SaveMenu m_saveMenu;

	void Start()
	{
		m_saveMenu = FindObjectOfType<SaveMenu> ();
	}
	
	public void  LoadLevel(string lvlName)
	{
		object[] args = new object[5];
		args [0] = lvlName;

		args [1] = m_saveMenu.player.currentData.name;

		switch(lvlName)
		{
		case "Test":
			args[2] = -20f;
			args[3] = 0f;
			args[4] = -2f;
			break;
		case "Lobby":
			args[2] = -142f;
			args[3] = -35f;
			args[4] = -2f;
			break;
		}

		 m_saveMenu.StartCoroutine ("ChangeLevel",args);
	}
	
	void OnTriggerEnter2D(Collider2D col)
	{
		if(col.GetComponent<Movement>())
		{
			m_saveMenu.SaveGame();
			LoadLevel(lvlName);
		}
	}
}
﻿using UnityEngine;
using System.Collections;

public class MoveObject : MonoBehaviour {
	
	[SerializeField]
	private GameObject objectToMove;
	
	[SerializeField]
	private GameObject triggerer;

	[SerializeField]
	private int speed;

	[SerializeField]
	private bool moveUp;

	[SerializeField]
	private bool triggerOnce = true;

	[SerializeField]
	private bool isSwitchTrigger;

	private bool switchTriggered;


	private bool isMoving;
	private bool isTriggered;

	[SerializeField]
	private float movingTime;

	public int triggerValue;
	public int progressIncrease;

	void Move()
	{
		if(moveUp) {
			if(switchTriggered)
			{
				objectToMove.transform.Translate(Vector2.up * -speed * Time.deltaTime);
				Vector2 velocity = Vector2.up * -speed;
				objectToMove.GetComponent<MovingPlatform>().velocity = velocity;
			}
			else
			{
				objectToMove.transform.Translate(Vector2.up * speed * Time.deltaTime);
				Vector2 velocity = Vector2.up * speed;
				objectToMove.GetComponent<MovingPlatform>().velocity = velocity;
			}
		}
		else {
			if(switchTriggered)
			{
				objectToMove.transform.Translate(Vector2.right * -speed * Time.deltaTime);
				Vector2 velocity = Vector2.right * -speed;
				objectToMove.GetComponent<MovingPlatform>().velocity = velocity;
			}
			else
			{
				objectToMove.transform.Translate(Vector2.right * speed * Time.deltaTime);
				Vector2 velocity = Vector2.right * speed;
				objectToMove.GetComponent<MovingPlatform>().velocity = velocity;
			}
		}
	}

	void FixedUpdate () { 

		if(isMoving)
		{
			if(isSwitchTrigger) {
				switchTriggered = true;
			}
			Move();
		}
	}
	
	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.transform == triggerer.transform)
		{
			if(isTriggered == false || triggerOnce == false && isMoving == false)
			{
				FindObjectOfType<ProgressManager>().AddProgress(progressIncrease);
				DoTrigger();
				//Debug.Log("a");
			}
		}
	}

	IEnumerator MoveTimer(float time) { 
		isTriggered = true;
		isMoving = true;
		yield return new WaitForSeconds(time);
		if(isSwitchTrigger)
			switchTriggered = true;
		isMoving = false;
		objectToMove.GetComponent<MovingPlatform>().velocity = Vector2.zero;
	}

	public void DoTrigger()
	{
		StartCoroutine(MoveTimer(movingTime));
	}
}
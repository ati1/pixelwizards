﻿using UnityEngine;
using System.Collections;

public class MoveRepeat : MonoBehaviour {

	[SerializeField]
	private GameObject objectToMove;

	
	[SerializeField]
	private int speed;
	
	[SerializeField]
	private bool moveUp;

	[SerializeField]
	private float movingTime;

	[SerializeField]
	private int moveDir;




	// Use this for initialization
	void Start () {
		StartCoroutine("ChangeDir", movingTime);
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		Move();

	
	}

	void Move()
	{
		if(moveUp) {
			if(moveDir == 0)
			{
				objectToMove.transform.Translate(Vector2.up * speed * Time.deltaTime);
				Vector2 velocity = Vector2.up * speed;
				objectToMove.GetComponent<MovingPlatform>().velocity = velocity;
			}
			else
			{
				objectToMove.transform.Translate(Vector2.up * -speed * Time.deltaTime);
				Vector2 velocity = Vector2.up * -speed;
				objectToMove.GetComponent<MovingPlatform>().velocity = velocity;
			}
		}
		else {
			if(moveDir == 0)
			{
				objectToMove.transform.Translate(Vector2.right * speed * Time.deltaTime);
				Vector2 velocity = Vector2.right * speed;
				objectToMove.GetComponent<MovingPlatform>().velocity = velocity;
			}
			else
			{
				objectToMove.transform.Translate(Vector2.right * -speed * Time.deltaTime);
				Vector2 velocity = Vector2.right * -speed;
				objectToMove.GetComponent<MovingPlatform>().velocity = velocity;
			}
		}
	}


	IEnumerator ChangeDir(float time)
	{
		yield return new WaitForSeconds(time);
		if(moveDir == 1)
			moveDir = 0;
		else
			moveDir = 1;

		StartCoroutine("ChangeDir", movingTime);

	}
}

﻿using UnityEngine;
using System.Collections;

public class ShrinkObject : MonoBehaviour {
	
	[SerializeField]
	private GameObject objectToScale;

	[SerializeField]
	private GameObject triggerer;

	[SerializeField]
	private int size;

	public int progressValue;
	public int progressIncrease;
	
	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.transform == triggerer.transform)
		{
			FindObjectOfType<ProgressManager>().AddProgress(progressIncrease);
			Scale();
		}
	}

	public void Scale()
	{
		objectToScale.transform.localScale = new Vector2(size, size);
	}
}

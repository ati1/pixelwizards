﻿using UnityEngine;
using System.Collections;

public class TeleportPlayer : MonoBehaviour {

	[SerializeField]
	private GameObject player;

	[SerializeField]
	private GameObject teleportToObject;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.transform == player.transform)
		{
			player.transform.position = teleportToObject.transform.position;
		}
	}
}

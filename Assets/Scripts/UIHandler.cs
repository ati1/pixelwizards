﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class UIHandler : MonoBehaviour {

	public bool enableGUI;
	public static bool isAnyActive;

	[System.Serializable]
	public class UIGrp
	{
		public GameObject uiGrp;
		public KeyCode toggleKey;
		public bool isActive;

		public void SetActive()
		{
			if(!isActive)
			{
				uiGrp.SetActive(true);
				isAnyActive = true;
				isActive = true;
			}
			else
			{
				uiGrp.SetActive(false);
				isAnyActive = false;
				isActive = false;
			}
		}
	}

	public List<UIGrp> Uigrps;

	void Awake()
	{
		foreach(UIGrp grp in Uigrps)
		{
			if(grp.uiGrp.name == "UI")
			{
				grp.SetActive();
			}
			else
			{
				grp.isActive = true;
				grp.SetActive();
			}

			if(grp.uiGrp.name == "Inventory")
			{
				grp.SetActive();
				grp.isActive = true;
				grp.SetActive();
			}
		}
	}

	void Update()
	{
		if(enableGUI)
			ShowUI();
	}
	/// <summary>
	/// Shows the UI.
	/// </summary>
	void ShowUI()
	{
		foreach(UIGrp grp in Uigrps)
		{
			if(Input.GetKeyDown(grp.toggleKey))
			{
				grp.SetActive();
			}
		}

		if(Input.GetKeyDown(KeyCode.Escape))
		{
			foreach(UIGrp grp in Uigrps)
			{
				if(grp.uiGrp.name == "Inventory" || grp.uiGrp.name == "TalentSystem")
				{
					grp.isActive = false;
					grp.uiGrp.SetActive(false);
				}
			}
		}
	}
}